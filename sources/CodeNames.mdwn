This page contains a list of some NVIDIA chip code names and their corresponding official GeForce number. If you're running a recent version nouveau, you can find your chipset by doing `dmesg | grep -i chipset`. This will always be correct, whereas the lists below are approximate.

Be aware that this list may be outdated and incomplete. Nouveau aims to support all NVIDIA cards, but no effort is made to document which cards (and BIOSes) actually work, as this is deemed infeasible. Just try it and submit [[bug|Bugs]] reports if it doesn't work.  
You can look at the [[feature matrix|FeatureMatrix]] to get an idea on which features are supported for your card family.


## General code names
[[!table header="no" class="mointable" data="""
**Code name**  | **Official Name**  | **Nvidia 3D object codename**
[[NV04|CodeNames#NV04]]  |  Riva TNT, TNT2  | Fahrenheit
[[NV10|CodeNames#NV10]]  |  GeForce 256, GeForce 2, GeForce 4 MX  | Celsius
[[NV20|CodeNames#NV20]]  |  GeForce 3, GeForce 4 Ti  | Kelvin
[[NV30|CodeNames#NV30]]  |  GeForce 5 / GeForce FX  | Rankine
[[NV40|CodeNames#NV40]]  |  GeForce 6, GeForce 7  | Curie
[[NV50|CodeNames#NV50]]  |  GeForce 8, GeForce 9, GeForce 100, GeForce 200, GeForce 300  | Tesla
[[NVC0|CodeNames#NVC0]]  |  GeForce 400, GeForce 500  | Fermi
[[NVE0|CodeNames#NVE0]]  |  GeForce 600, GeForce 700, GeForce GTX Titan| Kepler
[[NV110|CodeNames#NV110]] | GeForce 750, GeForce 900 | Maxwell
[[NV130|CodeNames#NV130]] | GeForce 1060, GeForce 1070 | Pascal
[[NV140|CodeNames#NV140]] | NVIDIA Titan V | Volta
[[NV160|CodeNames#NV160]] | GeForce RTX 2060, GeForce GTX 1660 | Turing
[[NV170|CodeNames#NV170]] | GeForce RTX 3060, GeForce RTX 3070 | Ampere
"""]]

A code name ending with "GL" (for example: "NV30GL") indicates a professional-class (Quadro/Tesla) board.

## Detailed code names


### Ancient

Not supported by nouveau. 
[[!table header="no" class="mointable" data="""
**Code name**  | **Official Name** 
NV01  |  Diamond Edge 3D | _Supported by [[vesa|http://www.x.org/wiki/vesa]] driver_ 
NV02  |  _Never completed_ |
NV03  |  Riva 128  | _Supported by [[nv|http://www.x.org/wiki/nv]] driver_ 
"""]]

The cards are working in Linux, but with limited capabilities. Compiz and 3D do not work. There's also no proprietary driver. <a name="NV04"></a> 
### NV04 family (Fahrenheit)

First family supporting DMA FIFOs. 3d engine only supports drawing textured or 2-textured triangles, without hardware TCL. 
[[!table header="no" class="mointable" data="""
**Code name**  | **Official Name** 
NV04  |  Riva TNT
NV05  |  Riva TNT2 
NV0A  |  Aladdin TNT2 IGP 
"""]]

<a name="NV10"></a> 
### NV10 family (Celsius)

Added support for hardware TCL and a lot of other 3d features, enabling fully accelerated OpenGL 1.2. 
[[!table header="no" class="mointable" data="""
**Code name**  | **Official Name** 
NV10  |  GeForce 256<br/>Quadro 
NV11  |  GeForce2 Go, MX <br/>Quadro2 (EX, MXR) 
NV15  |  GeForce2 GTS, Pro, Ti, Ultra<br/>Quadro2 Pro
NV17  |  GeForce4 MX 420, MX 440, MX 440-SE (AGP 4x), MX 460<br/>Quadro4 500 XGL, 550 XGL, Quadro NVS (100, 200) 
NV18  |  GeForce4 MX 440-SE (AGP 8x), MX 440-8x, MX 4000, 420 Go, 440 Go, 448 Go, 460 Go, 488 Go, GeForce PCX 4300<br/>Quadro4 380 XGL, 580 XGL, Quadro NVS (50, 280, 400)
NV1A  |  GeForce2 IGP 
NV1F  |  GeForce4 MX IGP 
"""]]

NV19 is a marketing name for an NV18 card with a PCIe bridge chip. <a name="NV20"></a> 
### NV20 family (Kelvin)

Introduced basic shaders and hardware context-switching. 
[[!table header="no" class="mointable" data="""
**Code name**  | **Official Name** 
NV20  |  GeForce3 (Ti), Quadro DCC 
NV25  |  GeForce4 Ti 4200, Ti 4400, Ti 4600<br/>Quadro4 700 XGL, 750 XGL, 900 XGL 
NV28  |  GeForce4 Ti 4200-8X, Ti 4800 (SE), 4200 Go<br/>Quadro4 780 XGL, 980 XGL 
NV2A  |  XBOX GPU 
"""]]

<a name="NV30"></a> 
### NV30 family (Rankine)

Has support for both vertex programs and fragment programs, in addition to fixed pipe engine. 
[[!table header="no" class="mointable" data="""
**Code name**  | **Official Name** 
NV30  |  GeForce FX 5800 (Ultra)<br/>Quadro FX (1000, 2000) 
NV31  |  GeForce FX 5600 (Ultra, XT, Go)<br/>Quadro FX 700 
NV34  |  GeForce FX 5100 Go, 5200 (Ultra, Go), 5300, 5500, GeForce PCX 5300<br/>Quadro FX (330, 500, 600 PCI), NVS 280 
NV35  |  GeForce FX 5900 (ZT, XT, SE), 5950 Ultra, GeForce PCX 5900, 5950<br/>Quadro FX (1300, 3000, 3000G) 
NV36  |  GeForce FX 5700 (Ultra, VE, LE, Go), 5750, GeForce PCX 5750<br/>Quadro FX 1100 
"""]]

The NV37/39 codenames are marketing names for NV34/36 cards with a PCIe bridge chip. NV38 is a marketing name for an NV35 with a BIOS modification. <a name="NV40"></a> 
### NV40 family (Curie)

Only vertex programs and fragment programs, fixed pipe engine removed. 
[[!table header="no" class="mointable" data="""
**Code name**  | **Official Name** 
NV40  |  GeForce 6800 (Ultra, GT, GS, XT, LE, GTO)<br/>Quadro FX 4000 (SDI), Quadro FX 3400, 4400 
NV41  |  GeForce 6800 (XT, GTO, Go Ultra)<br/>Quadro FX 1400 
NV42  |  GeForce 6800 (GS, Go)<br/>Quadro FX (3450, 4000 SDI) 
NV43  |  GeForce 6200, 6500, 6600 (LE, GT, Go, Go TE, Go Ultra), 6700 XL<br/>Quadro FX (540, 540M, 550), NVS 440 
NV44  |  GeForce 6200 (TC, Go), 6250 Go, 6400 Go, 7100 GS<br/>Quadro NVS 285 
NV46 (G72)  |  GeForce 7200 (GS, Go), 7300 (LE, GS, Go), 7400 Go, 7500 <br/>Quadro FX 350(M), NVS (110M, 120M, 300M, 510M) 
NV47 (G70)  |  GeForce 7800 (GS, GT, GTX, Go, Go GTX)  <br/>Quadro FX 4500 (SDI, X2) 
NV49 (G71)  |  GeForce 7900 (GS, GT, GTO, GTX, GX2, Go, Go GTX), 7950 (GT, GX2, Go GTX)  <br/>Quadro FX (1500, 1500M, 3500, 5500, 550 SDI, 2500M, 3500M) 
NV4A (NV44A)  |  GeForce 6200 AGP 
NV4B (G73)  |  GeForce 7300 GT, 7600 (GS, GT, Go, Go GT), 7700 Go<br/>Quadro FX (550M, 560, 560M) 
NV4C (MCP61)  |  GeForce 6150LE / nForce 400/405, GeForce 6150SE<br/>Quadro NVS 210s / nForce 430 
NV4E (C51)  |  GeForce 6100 (Go) / nForce 410/430, 6150 (Go) / nForce 430 
NV63 (MCP73)  |  GeForce 7050/7100/7150 / nForce 630i 
NV67 (MCP67)  |  GeForce 7000M / nForce 610M, GeForce 7150M / nForce 630M 
NV68 (MCP68)  |  GeForce 7025/7050 / nForce 630a 
"""]]

<a name="NV50"></a> 
### NV50 family (Tesla)

Has unified shader architecture, can do GPGPU and [[CUDA|https://github.com/pathscale/pscnv/wiki/nvidia_compute]], has virtual memory, quite different from previous cards. 
[[!table header="no" class="mointable" data="""
**Code name**  |  **Official Name** 
NV50 (G80)  |  GeForce 8800 (GTS, GTX, Ultra)<br/>Quadro FX (4600 (SDI), 5600) 
NV84 (G84)  |  GeForce 8600 (GT, GTS, M GT, M GS), 8700M GT, GeForce 9500M GS, 9650M GS  <br/>Quadro FX (370, 570, 570M, 1600M, 1700), NVS 320M 
NV86 (G86)  |  GeForce 8300 GS, 8400 (GS, M G, M GS, M GT), 8500 GT, GeForce 9300M G  <br/>Quadro FX 360M, NVS (130M, 135M, 140M, 290) 
NV92 (G92)  |  GeForce 8800 (GT, GS, GTS 512, M GTS, M GTX)  <br/> GeForce 9600 GSO, 9800 (GT, GTX, GTX+, GX2, M GT, M GTX)  <br/> GeForce GTS 150(M), GTS 160M, GTS 240, GTS 250, GTX (260M, 280M, 285M), GT (330, 340)  <br/> Quadro FX (2800M, 3600M, 3700, 3700M, 3800M, 4700 X2), VX 200 
NV94 (G94)  |  GeForce 9600 (GSO 512, GT, S), 9700M GTS, 9800M GTS, GeForce G 110M, GT 130(M), GT 140  <br/>Quadro FX (1800, 2700M) 
NV96 (G96)  |  GeForce 9400 GT, 9500 (GT, M G), 9600 (M GS, M GT), 9650M GT, 9700M GT  <br/> GeForce G 102M, GT 120  <br/> Quadro FX (380, 580, 770M, 1700M) 
NV98 (G98)  |  GeForce 8400 GS, GeForce 9200M GS, 9300 (GE, GS, M GS)<br/> GeForce G 100, G 105M <br/>Quadro FX (370 LP, 370M), NVS (150M, 160M, 295, 420, 450) 
NVA0 (GT200)  |  GeForce GTX (260, 275, 280, 285, 295)  <br/>Quadro CX, FX (3800, 4800, 5800) 
NVA3 (GT215)  |  GeForce GT (240, 320, 335M), GTS (250M, 260M, 350M, 360M) <br/>Quadro FX 1800M 
NVA5 (GT216)  |  GeForce GT (220, 230M, 240M, 325M, 330M), 315  <br/>Quadro 400, FX 880M, NVS 5100M 
NVA8 (GT218)  |  GeForce 8400 GS, ION 2, GeForce 205, 210, G 210M, 305M, 310(M), 405  <br/>Quadro FX (380 LP, 380M), NVS (300, 2100M, 3100M) 
NVAA (MCP77/MCP78)  |  GeForce 8100, 8200, 8300 mGPU / nForce 700a series, 8200M G 
NVAC (MCP79/MCP7A)  |  ION, GeForce 9300, 9400 mGPU / nForce 700i series, 8200M G, 9100M, 9400M (G) 
NVAF (MCP89)  |  GeForce 320M 
"""]]

<a name="NVC0"></a> 
### NVC0 family (Fermi)

All sorts of fun. Feature-wise it isn't too different but the architecture has changed a lot.  
 These cards are generally working with the latest kernel and Mesa but may still have power management issues. It is recommended to use the Linux 3.1 kernel or newer (or a backported driver from this kernel).  
 Note: GeForce 405 does not belong to this family. 
[[!table header="no" class="mointable" data="""
**Code name**  |  **Official Name** 
NVC0 (GF100)  |  GeForce GTX (465, 470, 480, 480M)<br/>Quadro 4000, 5000[M] (??), 6000 
NVC1 (GF108)  |  GeForce GT (415M, 420, 420M, 425M, 430, 435M, 520M, 525M, 530, 540M, 550M, 555M, 620, 630M, 635M, 640M LE)<br/>Quadro 600, 1000M 
NVC3 (GF106)  |  GeForce GT (440, 445M, 545, 555M, 630M, 635M), GTS 450, GTX 460M <br/>Quadro 2000 (D), 2000M 
NVC4 (GF104)  |  GeForce GTX (460, 460 SE, 470M, 485M) <br/>Quadro 5000M (??)
NVC8 (GF110)  |  GeForce GTX (560 Ti OEM, 570, 580, 590)<br/>Quadro 3000M, 4000M, 5010M 
NVCE (GF114)  |  GeForce GTX (460 v2, 560, 560 Ti, 570M, 580M, 670M, 675M) 
NVCF (GF116)  |  GeForce GTS 450 v2, GTX (550 Ti, 560M) 
NVD7 (GF117)  |  Geforce GT 620M, 625M, (some) 630M, 710M, 720M
NVD9 (GF119)  |  GeForce 410M, 510 (?), GT (520, 520M, 520MX, 610), 610M <br/>Quadro NVS 4200M 
"""]]

<a name="NVE0"></a> 
### NVE0 family (Kepler)

First family to support using 4 monitors simultaneously on one GPU, older generations had only 2 CRTCs. 
[[!table header="no" class="mointable" data="""
**Code name**  |  **Official Name** 
NVE4 (GK104)  |  GeForce GTX (660 Ti, 670[M], 680[M], 690, 760, 760 Ti, 770, 775M, 780M, 860M) <br/>Quadro K3000[M], K3100M, K4000[M], K4100[M], K5000[M], K5100M, Tesla K10
NVE7 (GK107)  |  GeForce GT (640[M], 645M, 650M, 710M, 720M, 730M, 740[M], 745M, 750M, 755M), GTX (650, 660M) <br/>Quadro 410, K500[M], K600, K1000[M], K1100M, K2000[M], NVS 510, 1000
NVE6 (GK106)  |  GeForce GTX (645, 650 Ti, 660, 760M, 765M, 770M) <br/>Quadro K2100M, K4000
NVF0 (GK110)  |  GeForce GTX 780, Titan<br/>Tesla K20, Quadro K6000
NVF1 (GK110B) |  GeForce GTX 780 Ti, Titan Z<br/>Tesla K40
NV106 (GK208B) | GeForce GT 720
NV108 (GK208) |  GeForce GT 630, 635, 640, 710M, 720M, 730M, 735M, 740M, 920M <br/>Quadro K510M, K610M
NVEA (GK20A) | Tegra K1
NV??? (GK210) | Tesla K80
"""]]

Some 6xx & 7xx series cards are from the NVC0 family instead.

<a name="NV110"></a>
### NV110 family (Maxwell)
[[!table header="no" class="mointable" data="""
**Code name**  |  **Official Name** 
NV117 (GM107)  |  GeForce GTX (745, 750, 750 Ti, 840M, 845M, 850M, 860M, 950M, 960M) <br/>Quadro K620, K1200, K2200, M1000M, M1200M; GRID M30, M40
NV118 (GM108)  |  GeForce 830M, 840M, 930M, 940M[X]
NV120 (GM200)  |  GeForce GTX Titan X
NV124 (GM204)  |  GeForce GTX (970, 980)
NV126 (GM206)  |  GeForce GTX (950, 960)
NV12B (GM20B)  |  Tegra X1
"""]]

<a name="NV130"></a>
### NV130 family (Pascal)
[[!table header="no" class="mointable" data="""
**Code name**  |  **Official Name** 
NV132 (GP102)  |  NVIDIA Titan (X, Xp), GeForce GTX 1080 Ti
NV134 (GP104)  |  GeForce GTX (1070, 1080)
NV136 (GP106)  |  GeForce GTX 1060
NV137 (GP107)  |  GeForce GTX (1050, 1050 Ti)
NV138 (GP108)  |  GeForce GT 1030
"""]]

<a name="NV140"></a>
### NV140 family (Volta)
[[!table header="no" class="mointable" data="""
**Code name**  |  **Official Name** 
NV140 (GV100)  |  NVIDIA Titan V, NVIDIA Quadro GV100
"""]]

<a name="NV160"></a>
### NV160 family (Turing)
[[!table header="no" class="mointable" data="""
**Code name**  |  **Official Name** 
NV162 (TU102)  |  NVIDIA Titan RTX, GeForce RTX 2080 Ti
NV164 (TU104)  |  GeForce RTX (2070 Super, 2080, 2080 Super)
NV166 (TU106)  |  GeForce RTX (2060, 2060 Super, 2070)
NV168 (TU116)  |  GeForce GTX (1650 Super, 1660, 1660 Ti, 1660 Super)
NV167 (TU117)  |  GeForce GTX 1650
"""]]

<a name="NV170"></a>
### NV170 family (Ampere)
[[!table header="no" class="mointable" data="""
**Code name**  |  **Official Name** 
NV172 (GA102)  |  GeForce RTX (3080, 3090)
NV174 (GA104)  |  GeForce RTX (3060 Ti, 3070, 3080 Mobile)
"""]]

