## The Nouveau 3D Drivers

The 3D drivers of the Nouveau driver suite are in Mesa. All the drivers are in a state to allow to play some games, more or less. You can try them at your own discretion, and some distributions even package them. When you encounter problems with them, read below, whether bug reports are valued or accepted, please. [[InstallNouveau]] explains the installation process. 

While the drivers include a significant amount of functionality and probably work on the common Linux games shipped with distributions, there are still some serious issues and they are not too optimized for performance. OpenGL applications that work at first may crash sooner or later. Some applications may crash X itself or hang the GPU. You should be prepared to face issues, and be able to recover from them yourself. If the GPU hangs, you can either reboot or suspend (see [[LockupRecovery|LockupRecovery]]). 


## About bugs and problems

**Do not report build or compilation failures.** Those failures are either your own mistake, or they will be noticed soon anyway. You should follow [[TroubleShooting|TroubleShooting]], most likely you have an outdated libdrm or kernel. 

The developers are not interested to help you with installing the 3D drivers. Your fellow users on the other hand might. Notice, that the irc-channel #nouveau in OFTC is supposed to be a developer channel, so asking there will annoy people.

If you get a misrendered image using a Gallium3D-based driver, compare it with the software rasterizers (`softpipe` and `llvmpipe` drivers). If the software rasterizer has the same problem, do not submit a bug against Nouveau, because it is a generic Gallium3D bug. 

If you cannot get 2D acceleration to work, you will not get 3D acceleration, either. They both use the same hardware engines. Make sure 2D acceleration works first. 

If the driver appears to be really really slow, verify that you're not still using software rendering. Don't forget that you need a 32 bit version of mesa/nouveau (and dependencies) for running 32 bit applications (wine is a common one). 


## The drivers for card families

The Gallium3D-based drivers are all compiled into `nouveau_dri.so`. The correct library (`nouveau_vieux_dri.so` vs. `nouveau_dri.so`) and the right driver are automatically selected runtime, if they are available. 

These are the different 3D drivers for the [[Nvidia card families|CodeNames]] (for source code and history, see [[Source|Source]]): 


### nv50 and nvc0

Make sure you are using at least Mesa 7.11, but please consider using latest stable release. You have to use **at least the 2.6.38 kernel** or you will encounter memory corruptions. For nvc0 hardware it's recommended to use at least kernel 3.1. 

Bug reports for nv50 and nvc0 can be submitted by following the [[general reporting guidelines|Bugs]]. Please, also follow the _About bugs and problems_ section above. 


### nv30 (NV30 - NV40)

Rewritten Gallium3D driver for Geforce FX - GeForce 7000 series of cards, introduced into Mesa 9.0 by [[this commit|http://cgit.freedesktop.org/mesa/mesa/commit/?id=a2fc42b899de22273c1df96091bfb5c636075cb0]] on Apr 13th, 2012. 

Bug reports for nv30 can be submitted by following the [[general reporting guidelines|Bugs]]. Please, also follow the _About bugs and problems_ section above. 


### Removed: nvfx (NV30 - NV40)

Nvfx is a now deleted Gallium3D driver for Geforce FX - GeForce 7000 series of cards, replaced with a new driver nv30. Mesa versions before 9.0 have Nvfx. Please don't file bugs about the old nvfx driver. 


### nouveau_vieux (NV04 - NV20)

`nouveau_vieux_dri.so` is a classic Mesa DRI driver, and not a Gallium3D driver, because these cards do not have enough shader capabilities to reasonably support the Gallium3D infrastructure. 

Do not file bug reports about this driver. 
