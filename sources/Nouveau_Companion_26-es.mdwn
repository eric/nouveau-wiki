[[!table header="no" class="mointable" data="""
 [[Home|FrontPage]]  |  [[TiNDC 2006|IrcChatLogs-fr]]  |  [[TiNDC 2007|IrcChatLogs-fr]]  |  [[Old Logs|IrcChatLogs]]  |  [[Current Logs|IrcChatLogs]]  | DE/[[EN|Nouveau_Companion_26]]/[[ES|Nouveau_Companion_26-es]]/[[FR|Nouveau_Companion_26-fr]]/RU/[[Team|Translation_Team]] 
"""]]


## Informe irregular sobre el Desarrollo de Nouveau


## Edición del 25 de agosto


### Introducción

Esta es la primera versión del TiNDC que se publica antes en Phoronix.com. Hacemos esto por varias razones: 

* tener más repercusión y, tal vez, atraer a más desarrolladores. 
* lograr más eco sobre nuestra estado como proyecto. Parece que los TiNDC no son leídos tanto como nos gustaría. 
* más exposición significa un público mejor informado, lo que ojalá se traslade en más y mejores contribuciones. 
¿Qué cambia? Exceptuando el sitio web en el que lo publicamos primero, nada. Phoronix.com publicará antes la versión inglesa y, unos 2-3 días más tarde, aparecerá la versión del wiki, incluídas las traducciones. 

Por tanto, gracias a Phoronix.com por darnos la oportunidad de llegar a una mayor audiencia. 

Para quienes estén leyendo el TiNDC por vez primera: El informe irregular sobre el Desarrollo de Nouveau (TiNDC) es un resumen libre sobre los desarrollos en marcha en el proyecto nouveau. Incluye, fundamentalmente, la información del canal de IRC #nouveau en freenode.net además de la lista de correo y los cambios en git si es necesario. Si prefieres una versión traducida (francés y español), mirad en el wiki, ya que aparecerán en él en unos días. 


### Estado actual

La mayor parte del trabajo se lleva acabo en estos momentos en el 2D, y gran parte de él va a hacer funcionar Xv. Ahuillet y p0g han creado trazas de registros (MMIO) para YV12 y YUY2 y las compararon entre sí. A partir de los resultados filtraron los registros que se tocaban para inicializar el overlay (superposición de video). El 10.08.2007 anunciaron resultados y pidieron la realización de pruebas. dagb creó una matriz de funcionalidades en la que se indica qué funciona y qué está siendo desarrollado... o no. Puedes verla aquí: [[http://nouveau.freedesktop.org/wiki/FeatureMatrix|http://nouveau.freedesktop.org/wiki/FeatureMatrix]] 

Por cierto, tenemos los siguientes tipos de superposición de vídeo: 

* NV04/NV05 solamente trabaja con YUY2,  
* NV10+ trabaja con YUY2  
* NV17+ trabaja con YV12  
La superposición de vídeo funciona en NV1x y hasta NV3x con buen rendimiento si los vídeos usan el formato YV12. Este formato se convierte al vuelo al NV12 (formato de imagen), que esperan las tarjetas NVidia. Esto significa, naturalmente, que la superposición nativa con NV12 también funciona. 

La primera versión tenía un error en la carga del plano de color, lo que provocaba problemas (colores ausentes) si la ventana que mostraba el vídeo se situaba parcialmente fuera de pantalla. Las pruebas del overlay con YV12/NV12 acababan en corrupción de la imagen debido a límites en el ancho de banda si el vídeo era grande (al menos 1024 píxeles en horizontal). Ahuillet y p0g supusieron que esto se debía al uso de 2 planos por el formato. El controlador nv de Xorg realiza algunos trucos con los registros de video CRTC que no comprendemos bien. Ahuillet simplemente recortó la imagen a la zona visible en pantalla, para obtener el mismo resultado. Tras solucionar esto, ahuillet y p0g volvieron su atención al formato YUY2. 

La superposición de video en NV04 no funciona debido a que ahuillet no tiene en la actualidad dicha tarjeta. O tenemos alguna oferta, o usaremos ebay... Por lo que podremos ver soporte de overlay para NV04 en el futuro próximo. 

MJules probó en NV3x y funcionaba, y pq lo hizo en NV28 también sin ningún problema. Pero dcha informó de la corrupción de la imagen en NV11 (GForce 2 Go) que condujo a p0g y ahuillet a investigar el asunto. La explicación es que las tarjetas < NV17 no disponen de overlay YV12, y únicamente admiten YUY2.  Por lo que se desactivó la superposición de video YV12 para < NV17. 

Las tarjetas NV4x no disponen de overlay, sino que usan el blitter (copia de memoria) para realizar ese trabajo. Ahuillet también aceleró ese caso. 

Mientras jugueteaban con el driver, matc y p0g obtenía de vez en cuando un error INVALID_STATE emitido por la tarjeta. Ahuillet y p0g intentaron rastrearla pero se dieron cuenta inmediatamente de que el error se encontraba en el código de EXA. El rastreo hasta la versión del DRM 0.0.4 parecía apoyar la idea de que el problema estaba ahí desde el principio de los tiempos (bien, por lo menos desde que empezamos), por lo que se hacía necesaria otra estrategia. Tras algunas pruebas, ahuillet y p0g se dieron cuenta de que la realización de algunas mezclas (blending) provocaban el error. Xv era únicamente una víctima que caía debido a un error de EXA que sucedía justo antes de ser llamada. 

Con la llegada del fin del Vacation of Code de ahuillet, querríamos agradecerle su trabajo atento que ha culminado en una implementación funcional de Xv para las tarjetas >=NV18 hasta las NV4x. Nos alegramos de que hayas elegido Nouveau para tu proyecto (y ¡nos alegramos más aún de que te quedes con nosotros!). 

pmdata finalmente logró crear volcados que contengan únicamente los cambios detectados sin interpretarlos, lo que hace los volcados mucho más pequeños. Para crear un volcado interpretado se pasa el volcado "en bruto" a través de renouveau-parse, que crea los conocidos volcados de texto. 

Ahora se guardan los datos del intérprete en una base de datos XML. El próximo paso sería modificar el generador de nouveau_drm.h para que usase también esa base de datos XML. En estos momentos se basa en el array nv_objects[] en renouveau, que contiene los mismos datos que la base de datos (bueno, no todos los datos se han trasladado, pero lo estarán pronto). 

kmeyer adaptó sus scripts de subida para que puedan trabajar con los archivos modificados que se envían por correo. Entretanto, JussiP anunció que no podría mantener la página de estado a partir del 1 de septiembre, ya que su cuenta va a ser desactivada. Estamos buscando otras opciones. 

cmn trabajó algo más en glxgears en NV35. La última noticia, entorno al Fosdem 2007, era que estaba funcionando pero con problemas en la matriz de proyección y ahora está arreglando poco a poco los problemas existentes de TCL (Transformación, Recortado e Iluminación). Pero queda todavía mucho que hacer. 

Si la altura es mayor que la anchura de la ventana, el resultado es un engranaje gigante quieto, sin posibilidad de ver el resto. 

Si el ancho es mayor que la altura de la ventana, los colores (fundamentalmente el azul) parpadean y algo remotamente comparable a un engranaje rojo aparece y desaparece, como si se moviese entorno a un punto. 

Por lo que, aunque el código ha mejorado mucho, el resultado (en comparación con lo esperado) todavía no es correcto. Pero comprobadlo por vuestra cuenta: 

[[[[!img http://www.cmartin.tk/nouveau/gear1.png]|http://www.cmartin.tk/nouveau/gear1.png]] [[[[!img http://www.cmartin.tk/nouveau/gear2.png]|http://www.cmartin.tk/nouveau/gear2.png]]  

Pero hay todavía una pega en la línea NV3x: la inicialización todavía no funciona correctamente. Por lo que arrancar X con Nouveau no llegaría demasiado lejos. Es necesario cargar el driver binario antes, ya que realiza alguna inicialización que en estos momentos hacemos incorrectamente, tras ello descargamos el driver binario, arrancamos las X, y nouveau funcionará. Eso singnifica simplemente que nuestro código de cambio de contexto funciona correctamente, pero falta al menos una cosa importante en el código de inicialización. Marcheu lo está investigando e intenta localizar qué pueda ser, pero necesita comprobar alrededor de 90MB de escrituras de registros en busca de los valores necesarios. Eso lleva tiempo (aunque ya solamente le quedan 250kb). 

Pequeñas notas: 

* El generador de nouveau_drm.h no funcionaba bien para el modo por defecto (creaba #defines inutilizables). Lo arregló KoalaBR 
* El script createdump.sh ganó algunos tests adicionales para detectar los archivos de cabecera de SDL y el archivo libXvMVNVIDIA.so, para que se pueda compilar renouveau.  
* pmdata implementó una instrucción de limpiado (clear) para todas las tarjetas NV04+ y NV1x. 
      * Por lo que ahora algunas partes de la pantalla realmente se limpian. Sin embargo, todavía falta que otros usuarios informen del funcionamiento. 
* Marcheu tuvo un fallo de disco duro y perdió alguno parches para NV04 que la hacían funcionar con el DRM actual. Básicamente arreglaban los cambios de contexto. Los rehará, pero, aún así, es toda una molestia. 

### Ayuda necesaria

Bien, gente, pongámonos serios aquí. ¿Queréis un driver de código abierto para NVidia que funcione? Bien. Necesitamos vuestra ayuda y esta sección es sobre cómo podéis salvar a un gatito. ¡Elige una tarea que te apetezca y ponte en contacto con nosotros! 

Por favor, probad Xv con nuestro driver. Solamente necesitais ver vuestro vídeo favorito con mplayer, codeine, etc, e informar a ahuillet de si funciona o no. 

Querríamos que los propietarios de tarjetas 8800 prueben nuestro drive e informen sobre ello. Ya que únicamente tenemos dos tarjetas G84 disponibles para el desarrollo y las pruebas, es muy necesaria la información que puedan proporcionar los usuarios  de este hardware. Por favor, fijaos en usar la rama randr-1.2 e informad a Darktama. 

Necesitamos trazas de MMIO para las tarjetas NV41, NV42, NV44,NV45, NV47,NV48 y NV4C. Y también necesitamos propietarios de tarjetas NV30 de los siguientes tipos: 10de:030x /  0x10de:0x00fc / 0x10de:0x00fd / 0x10de:0x00fe. Contactad con Darktama (NV4x) o Marcheu (NV3x) en nuestro canal de IRC.  

Esto remata el número #26 y señala mi primer aniversario como miembro del proyecto. Aunque irregular, hemos logrado sacar un número cada 14 días de media durante el último año. El próximo número, sin embargo, probablemente se retrasará una semana o dos ya que estaré de vacaciones hasta el final de septiembre. Sin embargo, intentaremos contestar a la sesión de preguntas y respuestas del foro en las próximas dos semanas. 

[[<<< Edición anterior|Nouveau_Companion_25-es]] 
