## The irregular Nouveau-Development Companion


## Issue for September, 1st


### Intro

Hello and welcome to the second issue of TiNDC. While I am waiting for  Gentoo to mark both Xorg 7.1 and the newest NVidia driver as stable so that I too can start testing the new driver, I thought that creating a new issue of the TiNDC would be a good idea. Don't be fooled by the fact that you don't see much traffic regarding nouveau on the dri mailing list. We do discuss our problems on the IRC channel #nouveau on freenode.net and even there you may find times, where the channel is very quiet. 


### Current status

Slowly the focus is shifting away from renouveau to nouveau, the real aim of our work. Pmdata did some investigation on the objects which are created during setup and added his insights to the nv10_objects file in the doc directory. KoalaBR did some work on the display lists and added documentation accordingly. 

Although a bunch of commands are still unknown, finding out their use becomes harder, as more and more commands seems to be constant. That means that if they exist, they always have one or two different values. Now finding out what they do, isn't that easy and maybe we can find it out only by trial and error during later development on nouveau. 

As the intro already indicated, work for the nouveau driver is slowly picking up speed. Just after marcheu finished his first version of a memory manager for DDX, arlied informed darktama and marcheu that he had setup a git repository for the driver on  git://anongit.freedesktop.org/git/nouveau/xf86-video-nouveau/ and one for DRM on git://anongit.freedesktop.org/git/mesa/drm/  (Tree: nouveau-1). Please have a look at [[UserStatus too.|UserStatus]]  
[[!table header="no" class="mointable" data="""
Git was developed by Linus Torvalds after the distribution of the free bitkeeper version was stopped. Git is a version tracking, management and distribution tool, which is used for the Linux kernel, X11 and MESA development. You can find more information [[here|http://git.or.cz/]]
"""]]

Stillunknown volunteered to try out the driver as the first person aside from our three main developers (marcheu, darktama, arlied). He tried it, found some bugs and added some docs to our homepage on fd.o. Furthermore, he volunteered to maintain some experimental ebuilds for Gentoo (have a look [[here|GentooEbuilds]]). This ebuild was promptly broken, when arlied did a clean up of the source tree, deobfuscating at least some of the horrible misdeeds found in the original open source nv driver. 

**Note by stillunknown/madman2003:** I had forgotten to upload the new ebuilds which uses the git drm, instead of cvs. That caused some problems, which resulted in uploading the new ebuilds. It was not the result of cleanup by airlied. 

As already hinted on above, there were already some bugs found in nouveau (EXA broken, Xv broken, mapping of the card's memory regions incorrect, mouse cursor stays on screen after X11 shutdown if you are using vesafb). Some of the bugs were squashed as soon as they were found, but the mouse cursor bug remained elusive, although both darktama and stillunknown tried to find it. Marcheu's bug fixing regarding the crashes lead to the discovery that the CPU usage of the driver was going down by roughly a factor of 2. There was much rejoicing first and more dismay later, when we found out that we got refresh / redrawing problems on some cards. Still looking into it. 

This "flood" of bugs lead to the discussion, whether we should open up a bugtracker and if yes, where. While marcheu and stillunknown were in favor of the idea, arlied didn't see it as a good idea, because: 

* gives the impression of general usability  
* draws away attention (often by "me too" post) from the development to bug hunting. 
Curently there will be bugs, as some crucial parts are still missing while others are not fully tested yet. Nouveau does not only need 3d acceleration, but a good 2d foundation too. nv is ugly and slow, nv plus 3d tacked on wouldn't do justice to our name ("nouveau") and even worse, would make further development hard. darktama, marcheu and arlied are currently doing the grunt work: Not much to see, but very important for the future development. 

So we aim to deliver nouveau with a good design, some 2d acceleration and  maybe even dual head display in the first step. After that it is time to  add 3d to it. 

Just on how to technically proceed was discussed several times between darktama, marcheu and arlied. Things like context switching, what is exposed to user space, how to pin certain pages, to prevent them to be moved around, etc. We will see at a later time, in what direction development moves, but there doesn't seem to much disagreement between those three. 


### Help needed

Well, at least one person read the first issue of TiNDC and offered help: Rathann has access to some recent quadros and offered to create all  dumps on it - Thank you very much! Nevertheless, we are still looking for some older quadros, yes drop into #nouveau on irc.freenode.org and offer your help. 

Basically, what we need now are developers you know their way around X11 or driver programming for mesa, please do offer your help. But even if you are not a programmer, you can help: 

* Don't ask questions like "Is the driver working / When will it be ready?", the answer can be found here, if there is  anything else we want you to know, we will let you know, promised.  
* Some more testing would be nice.  
* Mac, Bsd and Sun developers would be welcome to, to get our code as multiplattform as possible. 
Well, that's it for today, I hope you enjoyed reading our companion. 

[[<<< Previous Issue|NouveauCompanion_1]] | [[Next Issue >>>|NouveauCompanion_3a]] 
