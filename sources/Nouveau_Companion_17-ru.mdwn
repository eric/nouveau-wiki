[[!table header="no" class="mointable" data="""
 [[Главная|FrontPage-ru]]  |  [[TiNDC 2006|IrcChatLogs]]  |  [[TiNDC 2007|IrcChatLogs]]  |  [[Старые логи|IrcChatLogs]]  |  [[Текущие логи|IrcChatLogs]]  | DE/[[EN|Nouveau_Companion_17]]/[[ES|Nouveau_Companion_17-es]]/[[FR|Nouveau_Companion_17-fr]]/RU/[[Team|Translation_Team]] 
"""]]


## The irregular Nouveau-Development companion


## Выпуск от 18 Апреля


### Введение

На прошшлой неделе я не опубликовал это, так как считал, что любое мое высказывание не будет воспринято серьёзно. Так что я решил подождать ещё неделю. Это решение было особенно удачно, поскольку за Март было мало новостей. 

24/25 марта был последним днём приёма заявок для участия в Google's SoC. У нас 4 или 5 заявок, которые сейчас обрабатываются. Так как мы не знаем как много мест нам дадут в проекте Xorg, мы не можем сказать, какие проекты будут реализованы. У нас есть следующие заявки: 

* nouveaufb: framebuffer driver compatible with accelerated X driver for nvidia cards (nouveau) 
* Xv support for Nouveau driver 
* OpenGL texturing support for the 'Nouveau' driver 
* Add Exa support to more operations 
* Patent-free S3TC compression implementation for Mesa (эта заявка проходит по проекту Mesa и состоит из предложений, выдвинутых marcheu) 
Итак у нас 4 заявки из 12ти которые пришли на Xorg.(по данным daniels). 

Относительно нашей идеи провести ликбез: Скорее всего мы это сделаем, хотя мы не уверены как это сделать. мы или напишем страницу в wiki и договоримся о времени встречи в IRC, где люди могут приходить и задавать вопросы, или мы будем обсуждать отдельные темы в IRC, отвечая на вопросы после обсуждения и выкладывая их на Wiki. На текущий момент мы склоняемся к первому варианту, но ещё ничего не решено точно. Следите за новостями. 

Выпуски TiNDC теперь переводятся как минимум на испанский (см №16), а также возможно и на другие языки. Стартовал перевод wiki на немецкий, русский и польский. Спасибо за ваш усердный труд, переводчики! 

(прим. пер.: нужны желающие помочь в переводе на русский. Достаточно просто зарегистрироваться и перевести что-нибудь. Попробуйте, вам понравится ;) ) 


### Текущий статус

Thunderbird и Skinkie ещё немного продвинулись в реверс-инжиниринге регистров TV-Out. Так как у них обоих мало времени, прогресс идёт медленно. Thunderbird теперь может контролировать такие вещи как настройку края экрана, позиции изображения и другие простые вещи. Но сама по себе настройка tvout очень сложна и у него нет представления как это работает. 

Мы уже немного говорили об этом в предыдущем выпуске, но за последнюю неделю всплыла новая информация: новый менеджер памяти называемый TTM работает. Now NVidia hardware is somewhat different or advanced from current design and we need to make sure that our needs are met too. (кто знает как правильно перевести - переведите плз) 

Очень краткое резюме железа от NVidia 

* До 32х контекстов на каждой карте (похоже, что на NV50 их 128) 
* Переключение контекстов делается через менеджер прерываний (до NV2x) и самой картой (начиная с NV3x). 
* Каждый контекст состоит из FIFO и требуемых объектов. FIFO содержит все команды, в то время как объекты несут информацию, необходимую для обработки команды и содержат возможный результат. 
* Объекты создаются в зависимости от задачи (т.е. обработка 2D и 3D видео) 
* Объекты DMA описывают область памяти и его свойства (тип, адрес, ограничения, типы доступа) 
* Контексты полностью отделены друг от друга. 
* Управление FIFO осуществляется из user space 
Всё это приводит к появлению ряда требований к TTM: 

* как нам реализовать разграничение буферов которые могут быть использованы в несколько разных потоков? 
* предпочтительно чтобы ядро (DRM) не трогал FIFO. 
* разграничение/соединение нескольких буферов за один проход. 
* так как железо делает некоторую часть работы по проверке входных данных нам следует только убедиться что буферы и их адреса верны. 
* TTM способен перемещать буферы, даже целиком убирать их из AGP/VRAM. 
Well, totte (Thomas Hellström, a drm hacker) dropped by in #nouveau and discussed exactly those issue with wallbraker, marcheu and darktama. Result was, that we would need a new ioctl() call in the drm which allows for validating / fencing buffers in the TTM. This ioctl() needs only to be called if a new texture is introduced by the OpenGL application, although this might not be absolutely true, as darktama is not quite sure if this would really work out. 

So currently we believe, that we need to use the TTM like this: 1. Pin all the buffers you're reading/writing to (called "validating" by the TTM) 2. Submit commands that are using the buffers 3. Emit fences so the ttm knows when it's safe to evict 

"Pinning" means telling the TTM that it can't move or evict the buffers because they're in use. A "fence" is just a counter basically. After  the commands are submitted you tell the TTM "After this fence reaches this value, the buffers aren't being used anymore" when you tell the ttm to validate buffers, it marks them as being in use then later on you associate a "fence object" with it once the fence object is signalled (the fence counter reaches the desired value), the TTM marks the buffer(s) as being unused. 

Stillunknown was fed up that is NV43 (which is the same type as KoalaBR's) still didn't work while KoalaBR's one did, so over the last 3 weeks he pestered darktama again and again to help him get his card working. Finally, Darktama gave in and requested kernel logs with DRM debug output, X logs and nouveau fifo tracing output. Those helped Darktama to create a fitting patch. Stillunknown hit a bug in the swtcl while the rest of the NV4x cards weren't hitting that code path. [[Source|http://gitweb.freedesktop.org/?p=mesa/mesa.git;a=commit;h=ea3d11a3d8901d650eb2a858ce30abae2d20d278]] One more success report came in from mg, who did get a NV44 running, only the context voodoo was needed. 

PQ did more work on his register description database in xml. He refined the design with z3ro, so that it will be useful for Radeon and Nouveau. However, he nearly went insane when darktama casually mentioned that there are registers which are auto incremented when used to write to other registers (like uploading the context voodoo). We nearly lost PQ there, but he quickly recovered when genius stroke him and he found an easy way to encode just that information into his xml schema. While at first only aimed at enhancing the mmio-parse output it now seems as if nvclock (long term) and radeon may use this tool too for their needs. 

abotezatu (who applied for SoC) researched the still remaining NV1x issue regarding the non working context switch. 

Airlied continues his work on randr12 slowly due to other obligations. His branch is probably mostly broken for everything except his setup. But as he is concetrating on both PPC and x86 both architecture should be equally broken. It seems as if it will be necessary to parse some Bios tables to gain all necessary data to make this work. For now parsing the table on NV4x works (earlier cards probably don't have this table), but darktama broke the drm binary compability, thus a new merge with the master branch is needed. But currently Airlied is working to get TTM and the Intel drivers into shape. 

jb17some continue his search to find out, why the objects in instance ram are sometimes not found on G70 and above. He has a much better understanding of why the crashes occur and where the context instance mem is stored. It looks like G70 and up cards only store the instancemem in the frame buffer and not the 16mb pci region 0. This theory was verified against reality on his 7900gs (by dumping the pci region 0 + frame buffer), comparing this results against some dumps from the dump repository. Further test on a 6150 seem to support the theorie that this is true for all cards >= NV4x which are using shared memory. 

KoalaBR published the script to automatically download, compile and run renouveau including creating a fitting archive from the output. You can find a link to it on the REnouveau Wiki page and currently here: [[http://www.ping.de/sites/koala/script/createdump.sh|http://www.ping.de/sites/koala/script/createdump.sh]] 

And before we ask you again for your help, here we have a basket of easter eggs (well, no real eggs, but we hope you like them nevertheless) 

* Marcheu fixed context switching on NV04. Glxgears still doesn't work as some bugfixes for the 3d setup are still missing. 
* Julienr and Marcheu fixed the crashes on Quadro FX 350M (PCIE) (see bug #10404 on Bugzilla). If you see crashes with Go7300/7400 or similar chips, you should retest and give us feedback. 
* Context switching seems to work on all cards now, but not all cards can run glxgears yet. On some cards, certain 3d commands in DMA or FIFO hangs. The reason for that is currently under investigation. 

### Нужна помощь

Мы заинтересованы в дампах SLI систем и дампах  Go7300/7400 (и пожалуйста отметьте в теме вашего письма, что это SLI дамп). Также приглашаем владельцы G80 тестировать патчи. 

И конечно же, нас интересуют дампы [[MmioTrace|MmioTrace]]. 

Владельцы Go7300/7400, протестируйте наш драйвер в 2D режиме. 

Итак, это был Пасхальный выпуск TiNDC. Около 3х дней работы, покрывающий 3 недели разработки, прочтённый (позвольте угадать?) за 3 минуты? Неважно ;) Я надеюсь, что вы получили удовольствие от его прочтения. 
