[[!table header="no" class="mointable" data="""
 [[Home|FrontPage]]  |  [[TiNDC 2006|IrcChatLogs]]  |  [[TiNDC 2007|IrcChatLogs]]  |  [[Old Logs|IrcChatLogs]]  |  [[Current Logs|IrcChatLogs]]  | DE/[[EN|Nouveau_Companion_18]]/ES/[[FR|Nouveau_Companion_18-fr]]/RU/[[Team|Translation_Team]] 
"""]]


## Informe irregular sobre el desarrollo de Nouveau


## Edición del 22 de Abril


### Introducción

No hay demasiado que contar sobre las dos últimas semanas, ya que estamos pendientes de la inclusión del gestor de memoria TTM en el DRM en forma utilizable. Sin él estamos básicamente bloqueados, ya que si usásemos el gestor de memoria antiguo o uno propio estaríamos duplicando el trabajo, aunque la espera no nos conduce a ningún lado. 

Google ha anunciado su elección de proyectos para el Summer of Code. Desgraciadamente, Nouveau no obtuvo ninguna plaza. 

Mientras cundía la desesperación, Egbert Eich, de Xog, entró en el canal y anunció el "Vacation of Code" de Xorg, que busca amparar los proyectos rechazados y, por tanto, también los nuestros. Los detalles todavía están por pulir, ya que esta es la primera vez que se hace, y los problemas se resolverán a medida que surjan. Sin embargo, ya se ha contactado e informado a todos los estudiantes que propusieron proyectos para Xorg y que fueron rechazados. 

A día 20.04.2007 uno de esos proyectos ya ha sido aceptado: soporte de Xv para Nouveau, por Ahuillet. También se aceptó el proyecto para crear un nuevo algoritmo compatible con el algoritmo de compresión de texturas S3TC (que está patentado). Éste beneficiará a todos los drivers, no solamente a nouveau. El anuncio oficial del programa se puede consultar aquí: [[http://summer.cs.pdx.edu/|http://summer.cs.pdx.edu/]] 

¡Enhorabuena a todos los participantes!. Y a aquellos cuyas propuestas han sido rechazadas dos veces: ¡lo sentimos!. De todos modos, si disponéis de tiempo, nos encantaría que siguiéseis trabajando en nouveau.. 

Respecto al dinero de la colecta: todavía está pendiente la realización de la transferencia debido a problemas por nuestra parte. Si marcheu acepta el dinero tendríamos que pagar una cantidad ingente de impuestos, salvo que interviniese una organización sin ánimo de lucro que realizase la operación. Sin embargo, a nosotros no nos resulta posible crear y gestionar una (el proyecto está compuesto únicamente por voluntarios/as que participan en él en su tiempo libre), por lo que estamos buscando una organización sin ánimo de lucro que nos adopte como subproyecto y maneje el dinero por nosotros. El proceso, de todos modos, avanza lentamente. 


### Estado actual

pq y z3ro prosiguieron su trabajo para refinar el schema XML rules-ng, y comenzaron a implementar y probar algunas cosas. Una ejecución preliminar de este trabajo, partiendo de una traza de MMIO, obtuvo alrededor de unas 820k líneas convertidas/anotadas. Puesto que pq está comprobando manualmente la corrección de cada línea, no podemos ofreceros todavía un resultado definitivo (empezó el 19.04.2007) :) 

jwstolk inició su tarea de localizar secuencias comunes de código en los volcados de trazas de MMIO. Esta desembocaría en la creación de macros, que no son más que nombres descriptivos para secuencias largas de escrituras (incomprensibles) a registros (incomprensibles al menos para los seguidores habituales del canal, ya que nuestros intrépidos líderes desensamblan de memoria dichas escrituras, convirtiéndolas en mnemónicos :) ). 

pq también remató headergen.py. Este script crea, a partir de la base de datos antes mencionada, un archivo de encabezado que incluye #define's de las direcciones, nombres y valores posibles de los registros conocidos. 

El fin de semana de Semana Santa (Pascua), hughsie y darktama intentaron hacer funcionar la tarjeta de hughsie, una 7300 Go (¡ejem!). Tras cierto trabajo sucio con el vudú (secuencia de microcódigo para la inicialización), darktama creó un parche que tuvo éxito. La tarjeta funciona, también glxgears. 

Y ahora una lista de acontecimientos reseñables del canal que pueden resumirse en una frase: * Se actualizó el script de KoalaBR para descargar, compilar y ejecutar renouveau tras recibir algunas sugerencias de los usuarios. * Se está elaborando un script que reúna toda la información relevante sobre el sistema tras un fallo de nouveau. El script está pendiente de revisión por parte de las altas instancias del proyecto. * Aeichner intentó portar nouveau a *BSD pero se detuvo debido a que la implementación de DRM en BSD está retrasada en cuanto a funcionalidad. * Darktama tiene un pequeño desensamblador que decodifica parcialmente los valores del vudú de contexto (microcódigo de inicialización de contexto). * austin1 hizo funcionar glxinfo y glxgears en su NV42. 

Parece que tenemos problemas con tarjetas anteriores a la NV40. A menudo obtenemos interrupciones PGRAPH_ERROR, por lo que parece que nuestra inicialización de contexto no es suficientemente buena. Marcheu le está echando un vistazo al asunto. 

stephan_2303 nos ayudó a hacer funcionar nouveau en una NV4B. Durante el proceso de comprobación de los parches se dedujo que los 4 bits superiores de NV03_PFIFO_RAMHT definen el tamaño de las tablas de traducción (hashes). Cada contexto de una tarjeta NVidia posee un conjunto de objetos asignados o creados para él. Para localizar dichos objetos se usa una tabla de traducción. Desafortunadamente, este año, se desactivó en renouveau una prueba importante que podría proporcionar información relacionada con este asunto ya que provocaba fallos frecuentes. Es probable que reactivemos esta prueba (y pq provocó un fallo en su sistema al hacer una prueba y debe empezar de nuevo a comparar los resultados de mmio-parse :) ) 

KoalaBR hizo algunas pruebas con juegos 2D (majesty y Railroad Tycoon 2) y localizó algunos problemas en la actualización de la pantalla de juego y de la ventana. Se producía una corrupción de los resultados de modo que no era posible visualizar nada reconocible. La investigación posterior condujo a determinar que el mezclado (blending) contenía errores y que desactivarlo conducía a resultados correctos. 

Nuestro ejemplo más significativo de perseverancia en este asunto es jb17some: tras seguir su avance en los dos últimos números del TiNDC, logró finalmente hacer funcionar el cambio de contexto en su NV49. El parche preciso es prácticamente igual al de la NV4B, exceptuando algunos pequeños ajustes. 

Más problemas con nuestra infraestructura: alcanzamos el límite máximo de espacio adjudicado para el almacenamiento de datos en [[SourceForge|SourceForge]]. Con ello se desbarataron las subidas de datos y tuvimos que desactivarlas. Si has enviado algún volcado últimamente, esa es la razón de que no aparezca debidamente actualizado en la página de JussiP. 

¿Recuerdas nuestra herramienta de ingeniería inversa renouveau? Bien. Tras haberla abandonado durante un tiempo, han sucedido algunas cosas últimamente: * Script auxiliar de KoalaBR, para descargar, compilar y ejecutar la herramienta automáticamente. * Las prueba de comprobación de registros interesantes se ha reactivado (tal como se mencionó más arriba) * Ahora se introduce información de versión en la salida 

Aparte de esto, estamos meditando sobre la conveniencia de separar renouveau en dos programas independientes: volcador e intérprete. El volcador simplemente obtendría la información binaria (en lugar de hacerlo en forma legible para las personas, como hace ahora), mientras que el intérprete tomaría esos datos y los tranformaría a una forma legible. Esto permitiría usar un esquema similar al que pq está implementando para los volcados de MMIO, y, además, los volcados generados serían mucho más pequeños. 

La rama de randr1.2 captó alguna atención en los últimos días. Se integraron los cambios de la rama principal, así como algunos otros añadidos. Poco después, pq hizo algunas pruebas con su configuración y localizó algunos problemas de visualización (colores incorrectos, desplazamiento de la pantalla a la derecha). Lo bueno es que Airlied tiene exactamente los mismos problemas en su sistema, por lo que, en comparación con pruebas anteriores, ha habido cierto progreso. Todavía queda mucho trabajo que hacer y se avanza lentamente. 


### Ayuda necesaria

Independientemente de los problemas con sourceforge, buscamos volcados de tarjetas SLI y 8x00. Agradeceríamos mucho la ayuda de alguien con ciertos conocimientos de programación y una tarjeta 8x00. 

También deseamos recibir volcados MMIO de tarjetas en las que glxgears no funcione correctamente. 

Y, si eres la persona que hizo funcionar su NV44, todavía esperamos de tu parche (creo que tu nick era "mg"). 
