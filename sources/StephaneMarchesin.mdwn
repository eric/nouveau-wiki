## Stephane Marchesin


### Hardware

* NV03 12d2:0018 (AGP, Dsub) 
* NV04 10de:0020 (AGP, 16MB, Dsub) 
* NV05 10de:0028 (AGP, 16MB, Dsub, 135/166 mhz) 
* NV06 10de:002c (PCI, 32MB, Dsub) 
* NV06 10de:002d (AGP, 32MB, Dsub) 
* NV10 10de:0101 (AGP, 32MB, Dsub) 
* NV11 10de:0110 (AGP, 32MB, Dsub, TVout) softquadroable into 10de:0113 
* NV11 10de:0110 (AGP, 32MB, Dsub) 
* NV15 10de:0150 (AGP, 32MB, Dsub, TVout, TVin) softquadroable into 10de:0153 
* NV18 10de:0185 (AGP, 64MB, Dsub) softquadroable into 10de:018b 
* NV20 10de:0201 (AGP, 64MB, Dsub, TVout) 
* NV28 10de:0281 (AGP, 128MB, Dsub, DVI, Tvout) softquadroable into 10de:0289 
* NV30GL 10de:0309 (AGP, 128MB, DVI, DVI) 
* NV34GL 10de:032a (PCI, 64MB) 
* NV44 10de:0221 (AGP, 128MB, Dsub, DVI) 
* G86 10de:0XXX (PCIE, 256MB, Dsub, DVI) 

### TODO

* see if the multisampling technique from geforce 2 can be used for hardware mipmap generation 
* Install an older driver on the nv28 (3xxx, 4xxx) to play with ext_vertex_weighting (figure out if it has a second modelview matrix for vertex weight stuff)                 
* run test_startup() on the nv18 in nv18 & nv18sgl modes 
* fix the multiple fifo free issue (it seems fifo_free is called multiple times) 
* look at the nv15 with tv-in. see if it has a bigger aperture, which would be where the tv in regs sit. 
* add support for a crash hook for ubuntu automatic bug reporting. 
* check that my tnt2m64 has proper detection of its vram amount 
* explore the usage of movnt* to write to PCI memory. This will avoid polluting the cache, both for DMA source memory and FIFO writes. This might explain the performance difference of PCI and AGP. 
