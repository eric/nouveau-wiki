[[!table header="no" class="mointable" data="""
 [[Accueil|FrontPage-fr]]  |  [[TiNDC 2006|IrcChatLogs-fr]]  |  [[TiNDC 2007|IrcChatLogs-fr]]  |  [[Archives anciennes|IrcChatLogs-fr]]  |  [[Archives récentes|IrcChatLogs-fr]]  | DE/[[EN|Nouveau_Companion_17]]/[[ES|Nouveau_Companion_17-es]]/FR/[[RU|Nouveau_Companion_17-ru]]/[[Team|Translation_Team]] 
"""]]


## Le compagnon irrégulier du développement de Nouveau (TiNDC)


## Édition du 8 avril 2007


### Introduction

Le dernier weekend (ndt: le 1er avril), je n'ai publié aucune mise à jour, car j'avais l'impression que aucun des sujets mentionnés n'aurait été pris au sérieux. J'ai donc juste attendu une semaine de plus. "And as March was slow going, this was fitting too." (ndt: je n'arrive pas vraiment à traduire cette phrase, j'ai donc préféré la laisser telle-quelle, si quelqu'un sait, qu'il n'hésite pas à contribuer!) 

Le weekend des 24 et 25 Mars a été la fin des candidatures au Google SoC. Nous avons environ 4-5 candidatures, qui sont actuellement étudiées. Puisque nous ne savons pas combien de parts nous avons dans le meta-projet Xorg pour SoC, nous ne pouvons pas dire pour l'instant quels projets seront réalisés. Nous avons des propositions pour : 

* nouveaufb: un driver framebuffer compatible avec le driver accéléré pour les cartes nVidia (Nouveau) 
* Le support de Xv pour le driver Nouveau 
* Le support des textures en OpenGL pour le driver Nouveau 
* Le rajout du support Exa à un plus grand nombre d'opérations 
* Une implémentation _Patent-free_ de la compression S3TC pour Mesa, cette proposition est pour Mesa et est due à une suggestion de marcheu 
Nous avons donc 4 candidatures pour un total de 12 qui sont arrivées pour Xorg (d'après daniels). 

Par rapport à notre idée de cours à propos du crash de driver: Nous allons probablement le faire, seulement nous ne sommes pas tout à fait sûr de la manière de le faire. Soit nous allons écrire une page de wiki et définir un certain horaire sur IRC où les gens pourront venir et poser des questions, ou nous discuterons à propos d'un sujet sur IRC, en laissant la possibilité de poser des questions plus tard, et écrirons sur le Wiki. Pour l'instant nous penchons pour la première options, mais continuez de surveiller ici. 

Le TiNDC est maintenant traduit au moins en espagnol (regardez [[le numéro 16|Nouveau_Companion_16-fr]]), peut-être dans d'autres langues aussi. Les traductions Allemandes, Russes et Polonaises des pages du Wiki ont débuté. Merci beaucoup pour tout ce dur labeur aux équipes de traduction! 


### État actuel

Thunderbird et Skinkie ont travaillé sur le _reverse engineering_ des registres pour la sortie TV. Étant donné qu'ils n'ont pas beaucoup de temps à consacrer à ça, la progression est lente. Mais actuellement Thunderbird est capable d'effectuer quelques choses de basiques telles que l' _overscan_, le déplacement de l'image ou ce genre de choses qui sont relativement simples à effectuer, mais configurer la sortie TV elle-même est bien plus compliqué, et il n'a aucune idée de la manière réelle dont cela marche. 

Cela a déjà été traité dans un ancien TiNDC, mais d'autres informations ont fait surface pendant les dernières semaines: un nouveau gestionnaire de mémoire appelé TTM est en chantier. Le matériel NVidia demande quelque chose de différent ou de plus avancé que l'architecture actuelle, et nous devons aussi nous assurer que nos besoins seront satisfaits. 

Juste un très rapide récapitulatif du matériel NVidia: 

* Jusqu'à 32 contextes matériels sur chaque carte (et il semblerait que le NV50 en ait 128) 
* Le changement de contexte est effectué par gestion d'interruptions (jusqu'au NV2x) ou par la carte elle-même (depuis le NV3x). 
* Chaque contexte consiste en une FIFO et les objets requis. La FIFO contient toutes les commandes pendant que les objets contiennent toutes les informations nécessaires pour traiter la commande et contenir les résultats possibles. 
* Les objets sont créés en fonction de la tâche à traiter (par exemple 2D, 3D, traitement vidéo) 
* Les objets DMA décrivent une région en mémoire et ses propriétés (type, adresse, limites permises, types d'accès permis) 
* Les contextes sont totalement séparés les uns des autres. 
* Les FIFOs sont gérées depuis l'espace utilisateur. 
Cela laisse donc quelques quelques exigences pour le driver Nouveau en ce qui concerne TTM: 

* Comment sommes-nous supposés implémenter le _fencing_ des buffers qui peuvent être utilisés sur plusieurs flux de commande séparés? 
* Il serait préférable que le noyau (DRM) ne mette pas la pagaille dans une FIFO. 
* _fencing_ / _pinning_ de plusieurs buffers à la fois. 
* Puisque le matériel effectue déjà plusieurs validations, nous devons juste nous assurer que les buffers et leurs adresses sont valides. 
* Le TTM est capable de déplacer des buffers, et même de les éliminer de l'AGP/la VRAM complètement. 
En fait, totte (Thomas Hellström, un développeur sur le drm), est passé sur #nouveau et a déjà discuté de ces points avec wallbraker, marcheu et darktama. Le résultat étant que nous aurons besoin d'un nouveau appel ioctl() dans le drm, qui permettra la validation et le _fencing_ des buffers dans le TTM. Cet ioctl() doit seulement être appelé si une nouvelle texture est introduite par l'application OpenGL, bien que cela peut ne pas être absolument vrai, puisque darktama n'est pas totalement sûr que cela puisse réellement marcher. 

Donc pour l'instant nous pensons que nous devons utiliser le TTM de cette manière: 

1. Effectuer le _pinning_ de tous les buffers dans lesquels nous lisons/écrivons (cela est appelé "validation" par le TTM) 
1. Envoyer toutes les commandes qui utilisent les buffers 
1. Émettre des _fences_ pour que le TTM sache quand cela ne pose plus de problème de libérer les _buffers_. 
Le _pinning_ est un moyen de dire au TTM qu'il ne peut pas bouger ou libérer un _buffer_ car il est utilisé. 

Le _fencing_ est juste un compteur. Une fois que les commandes sont envoyées, on dit au TTM "Après que ce compteur ait atteint cette valeur, les buffers ne seront plus utilisés" : 

* On dit tout d'abord au TTM de "valider" des _buffers_, il les marque comme étant utilisés. Ensuite on y associe un objet de type _fence_. Quand cet objet émet le signal (lorsque le compteur a atteint la valeur désirée), le TTM marque ces _buffers_ comme étant inutilisés. 
Stillunknown était étonné que son NV43 (qui est du même type que celui de KoalaBR) ne marche toujours par alors que celui de KoalaBR marche, donc pendant les 3 dernières semaines il a harcelé darktama encore et encore pour l'aider à faire marcher sa carte. Finalement, Darktama a demandé des logs du noyau avec la sortie debug du DRM, des logs de X et des traces des FIFOs de Nouveau. Tout cela l'a aidé à créer un patch approprié. Stillunknown est tombé sur un bogue dans le swtcl alors que le reste des cartes NV4x n'avaient pas ce problème. [[Source|http://gitweb.freedesktop.org/?p=mesa/mesa.git;a=commit;h=ea3d11a3d8901d650eb2a858ce30abae2d20d278]] Un rapport de succès supplémentaire est venu de mg, qui a réussi à faire marcher un NV44, il manquait juste la gestion du changement de contexte (ndt: appelé _context voodoo_ dans le texte original, car ce code est relativement complexe, obscur et tient de temps en temps presque de la magie). 

PQ a effectué plus de travail sur sa base de donnée XML de description des registres. Il a affiné le modèle avec z3ro pour qu'il puisse être utile pour Radeon et Nouveau. Seulement, cela est presque devenu dément lorsque Darktama a mentionné qu'il y avait des registres qui s'auto-incrémentaient lorsqu'ils étaient utilisés pour écrire dans d'autres registres (comme l'envoi du _context voodoo_). Nous avons presque perdu PQ à ce moment là, mais il s'en est rapidement remis lorsque son génie l'a frappé et qu'il a trouvé un moyen simple d'incorporer cette information dans son schéma XML. Alors que nous voulions uniquement améliorer la sortie de mmio-parse, il semble maintenant que nvclock (sur le long terme) et Radeon pourraient utiliser cet outil. 

abotezatu (qui s'est proposé pour le SoC) a travaillé sur le dernier problème à propos du changement de contexte qui ne fonctionnait pas sur le NV1x. 

Airlied a continué son travail sur randr12, lentement à cause de ses autres obligations. Sa branche est probablement presque totalement cassée pour à peu près tout sauf sa propre configuration. Mais puisqu'il se concentre à la fois sur PPC et x86, les deux architectures devraient être cassées de la même manière. Il semble qu'il sera nécessaire de parser quelques tables du Bios pour obtenir les données nécessaire pour que cela marche. Pour l'instant, parser ces tables sur les NV4x marche (les cartes plus récentes n'ont probablement pas cette table), mais Darktama a cassé la compatibilité DRM binaire, un nouveau _merge_ avec la branche principale est donc requise. Mais pour l'instant Airlied travaille afin de faire marcher TTM et les drivers Intel. 

jb17some continue ses recherches pour trouver pourquoi les objets dans la RAM d'instances ne sont parfois pas trouvés sur les G70 et supérieurs. Il a une bien meilleure compréhension sur pourquoi les crash arrivent et où le contexte de la mémoire d'instance est stocké. Il semblerait que les G70 et supérieures stockent uniquement la mémoire d'instance dans le _frame buffer_, et non dans la région PCI 0. Cette théorie a été vérifiée sur sa 7900gs (en un _dump_ de la région PCI 0 + _frame buffer_) et en comparant ces résultats par rapport à d'autres _dumps_. Des tests plus poussés sur une 6150 semble supporter la théorie comme quoi cela serait vrai pour toutes les cartes >= NV4x qui utilisent la mémoire partagée. 

KoalaBR a publié le script pour automatiquement télécharger, compiler et lancer REnouveau, incluant la création de l'archive _qui va bien_ à partir de la sortie. Vous pouvez trouver un lien sur la page Wiki de REnouveau, ou immédiatement ici: [[http://www.ping.de/sites/koala/script/createdump.sh|http://www.ping.de/sites/koala/script/createdump.sh]] 

Et avant de vous demander à nouveau votre aide, nous avons ici un panier d'œufs de pâques (bon, pas de vrais œufs mais nous espérons que vous les aimiez quand même) 

* Marcheu a réglé le changement de contexte sur NV04. Glxgears ne marche toujours pas et quelques corrections de bogues pour l'initialisation 3d manquent encore. 
* Julienr et Marcheu ont résolu les plantages sur Quadro FX 350M (PCI-e) (voir bug n°10404 sur le bugzilla). Si vous avez des plantages avec des Go7300/7400 ou des puces similaires, vous devriez retester et nous envoyer vos réactions. 
* Le changement de contexte semble à présent marcher sur toutes les cartes, mais toutes les cartes ne peuvent pas encore faire tourner glxgears. Sur certaines cartes, certaines commandes 3d dans le DMA ou la FIFO se bloquent. La raison à cela est encore en cours d'investigation. 

### Aide requise

Nous sommes intéressés par des _dumps_ de configurations SLI et de Go7300/7400 (et s'il-vous plaît, indiquez dans le sujet de votre email que ce sont des configuration SLI). Les possesseurs de G80 qui veulent tester des patchs sont aussi les bienvenus. 

Et oui, des _dumps_ [[MmioTrace|MmioTrace]] seraient aussi les bienvenus. 

Les possesseurs de Go7300/7400 peuvent tester si Nouveau (en 2d) marche pour eux. 

Voilà, c'était l'édition de Pâques du TiNDC. Environ 3 jours de travail, couvrant 3 semaines de développement, lu en environ (une estimation) 3 minutes? Pas grave :) En espérant que vous ayez apprécié de le lire. 

[[<<< Édition précédente|Nouveau_Companion_16-fr]] | [[Édition suivante >>>|Nouveau_Companion_18-fr]] 
