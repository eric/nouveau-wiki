[[!table header="no" class="mointable" data="""
 [[Home|FrontPage]]  |  [[TiNDC 2006|IrcChatLogs]]  |  [[TiNDC 2007|IrcChatLogs]]  |  [[Old Logs|IrcChatLogs]]  |  [[Current Logs|IrcChatLogs]]  | DE/EN/[[ES|Nouveau_Companion_30-es]]/[[FR|Nouveau_Companion_30-fr]]/RU/[[Team|Translation_Team]] 
"""]]


## The irregular Nouveau-Development companion


## Issue for November 11th


### Intro

And once again more than 2 weeks have passed and it is time for yet another issue of the TiNDC. 

So we are fighting to get a first "stable" release of nouveau out as soon as possible. This release will include 2D, Xv and EXA acceleration for all cards from NV05 to NV4x. Owners of NV04 won't get EXA due to hardware limitations,  while NV5x cards (GeForce 8x00) won't get much more than working 2D and hopefully working mode setting for the common cases. 

NV5x is seriously understaffed with only Darktama (who is limited in his time due to real life concerns) and two novice developers trying to get the hang of it. KoalaBR is basically restricted to some reverse engineering and testing due to real life concerns. 

We do often get reports that our driver is working slowly with gnome or KDE in that icon rendering seems to be delayed. All cases we got so far have been traced back to a xserver 1.3 installation. Moving to the recent xserver 1.4.1 fixes the issue. Please do not use xserver 1.4 which had some  issue with EXA and in the input department. 

BTW: for every new issue I do annoy different developers to check for factual  errors and typos. I would like to say "Thank you" for their help. 


### The Current Status

Marcheu fixed most outstanding issues with EXA NV3x and even got PPC working again. But still with composite there are outstanding bugs, one which makes text in Qt applications disappear. 

Next thing we want to do, is say "Thank you" [[IronPeter|IronPeter]] who is working to get 3D  working on the PS3 (a big endian machine). He did ask for help and as far as  possible, we were glad to help. So he did get 3D vertex and fragment shader programs to work. But best thing is: He found our problem with the missing glxgears colours too: fragment shaders need 32 bit constants swapped 16-bit wise (ABCD would be CDAB). Darktama immediately changed our code for DDX to reflect that and airlied did some testing  on his G5 and it worked. So NV4x EXA is now accelerated on PPC too! 

Seeing that, airlied added the bug fix to the DRI driver too. And this got rid of the  "no colours" gears. 

Later tests on different hardware however were conflicting for 2D: some said it worked,  some claimed crashes.  
 [[Airlied blog|http://airlied.livejournal.com/52707.html]]  
 [[IRC|http://people.freedesktop.org/~ahuillet/irclogs/nouveau-2007-11-01.htm#0758]]  
 [[DRI patch|http://gitweb.freedesktop.org/?p=mesa/mesa.git;a=commit;h=ee793281b221415f794af6aadaa9764023612e0b]]  
 [[DDX patch 1|http://gitweb.freedesktop.org/?p=nouveau/xf86-video-nouveau.git;a=commit;h=f59e596bcd90ef824cd41e0c37952e574d6914bb]] [[DRI patch 2|http://gitweb.freedesktop.org/?p=nouveau/xf86-video-nouveau.git;a=commit;h=5bd8ba52788b3b3b8f91ba59c29e154e38482481]] 

But still non-NV4x EXA was either not working on PPC or working slowly. So both  Airlied and Marcheu approached the PPC guru benh and asked for help. He  volunteered to have a look at the code and audit it for typical endian bugs. This work is still ongoing, but at least we didn't commit any obvious PPC crimes, even benh needs to take a deeper look. 

AndrewR tested EXA on NV11 and reported the same problem (missing fonts) on NV11. p0g investigated the issue and got confused with the values for X/Y  position on screen handed to nouveau by EXA. Later he found the problem and fixed it. 

Ahuillet and p0g tried to get the final hurdle for fast EXA on NV1x cleared:  Finding a way to implement the A8+A8 [[PictOp|PictOp]]. That proved to be difficult, as they couldn't find a hardware supported way to do it. Options were discussed (like convert to a known texture format, where to convert etc.), but no immediate solution came out of it. After a few tests which yielded problems if width, destination x/y positions weren't even, a discussion between ahuillet, p0g and marcheu started. Ahuillet's hacks were scrapped and an enhancement patch for EXA was sketched out. [[IRC|http://people.freedesktop.org/~marcheu/irclogs/nouveau-2007-11-07#1848]] 

So both tried various yet unknown values to the texture format in order to find a fitting texture mode but yet without luck. 

Pmdata and CareyM found out bugs in renouveau, which were leaking giving error / incorrect result for certain tests. CareyM even went back to driver versions 71xx to make sure it was not a bug in the driver (Thanks a lot for  this hard work!). Later pmdata found the culprit: An Enable(GL_TEXTURE_RECTANGLE_NV)  where it was not needed. He rewrote the test to squish this bug  Patch is [[here|http://nouveau.cvs.sourceforge.net/nouveau/renouveau/tests.c?r1=1.216&r2=1.217]]. 

stillunknown along with various contributors tried to get mode switching into shape. chowmeined had problems with incorrect timing leaving two pink lines at the bottom and right side of the display. He managed to eliminate those at the bottom but failed to fully get rid of the one on the right. Stillunknown tested supplied test patches and got it into a working state. 

Later malc0 joined the scene with a patch to parse the DCB in the videocard bios.  Furthermore the DDX saw many clean ups to the randr 1.2 and related code, so that development and maintenance should become easier. 
[[!table header="no" class="mointable" data="""
Insert: Display Configuration Block (DCB)  
 The DCB is a part of the videocard's bios and consists of a number of entries which include data interesting for mode setting. Each entry relates to a potential (possible) output like VGA, TV-OUT etc. Currently nouveau uses the data associated with the entries to know what output types are present, what I2C ports to use to get EDID data for each output and how the RAMDAC routing should be performed. 
"""]]

But later on stillunknown managed to cold init the second head for with randr1.2 which is one major problem less. [[patch|http://gitweb.freedesktop.org/?p=nouveau/xf86-video-nouveau.git;a=commit;h=8241710c94f6df0bf683bc3c93f7ea1ca14d118c]]   
 [[IRC|http://people.freedesktop.org/~marcheu/irclogs/nouveau-2007-11-06#1030]] 

pq did some testing on NV20 and supplied further information about his results.  Fiddling with various timing registers both managed to get NV20 into a working state too.  [[IRC|http://people.freedesktop.org/~marcheu/irclogs/nouveau-2007-11-03#2042]] 

jb17some presented his reverse engineering work on XvMC. He seems to be sure that he now understands the basic way of working. The FIFO sets up and controls rendering as usual, but the data is not uploaded via FIFO but via a separate buffer. This buffer is created / updated via calls to XVMCCreateContext(), XVMCCreateSurface(),  XVMCRenderSurface(). Whether he intends to give an implementation a shot remains to be seen. [[IRC|http://people.freedesktop.org/~marcheu/irclogs/nouveau-2007-11-03#2232]] 

With work on NV3x finished and marcheu fixing some of the remaining NV30-only bugs,  jkolb  set his eyes on a new challenge. After talking to marcheu, airlied and  darktama he decided to work a bit on Gallium and TTM. JKolb did a couple of commits,  first some stub BO driver functions that did nothing, then fleshed out a bit from airlied's radeon ttm code.  At that point darktama send  jkolb a diff of his TTM branch, and he added some bits from that  code that were missing from the BO driver, and committed darktama's fence driver.  At that point Darktama did a bit of work at fixing a few bugs which were found compared to the original code. 

To get a fleshed out Gallium3d / TTM framework much is missing though. The DRM is still missing some vital pieces ("superioctl", some bad bugs,  etc. etc), and we have our entire userspace code to adapt. This is is actually quite a lot of work! 

By the way: airlied has posted the new DRM including TTM to the LKML. That means that  the API should be finished and as stable as it can get under Linux. [[http://www.uwsg.iu.edu/hypermail/linux/kernel/0711.0/1178.html|http://www.uwsg.iu.edu/hypermail/linux/kernel/0711.0/1178.html]] 

So we now "only" need to flesh out the parts which are driving the 3D engines on the cards. This sounds much easier than done though, as marcheu needs to get his generic part on Gallium3D done, so that older cards without variable length shaders can handle the requests too. 

Some short topics: 

* We are looking for someone to write a "Do notifiers in AGP space work?" test for nouveau. Should the notifier not work, nouveau should use notifiers an PCI space. Basically pq and marcheu are both occupied but this patch would help quite a bit. 
* our code style policy is: Ident via tabs 8 characters wide plus additional spaces if indentation is smaller than 8 chars (counted from the last tab on the line). 
* The documentation of Xv for mmio-parse register was enhanced by hkBst. 

### Help needed

Please do test whether nouveau PPC on PPC works for you (in case you have that kind of hardware). 

Additionally, please have a look at the "Testers wanted" page for requirements coming up between our issues. [[http://nouveau.freedesktop.org/wiki/TestersWanted|http://nouveau.freedesktop.org/wiki/TestersWanted]] 

And finally a thanks to Luc Vernerey for sending us 2 cards (GeForce 3 and 4) 

[[<<Previous Issue|Nouveau_Companion_29]] [[Next Issue >>|Nouveau_Companion_31]] 
