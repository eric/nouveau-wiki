Video decoding hardware generation for each chipset: 
[[!table header="no" class="mointable" data="""
 **[[NV04|CodeNames]]** || | **VPE** | **VP1** | **VP2** | **VP3/VP4/VP5**
NV04|| |  |  |  | 
NV05|| |  |  |  | 
**[[NV10|CodeNames]]** || | VPE  | VP1  | VP2  | VP3/VP4/VP5
NV10|| |  |  |  | 
NV11|| |  |  |  | 
NV15|| |  |  |  | 
NV17|| | YES (v1) |  |  | 
NV18|| | YES (v1) |  |  | 
NV1A|| |  |  |  | 
NV1F|| | YES (v1) |  |  | 
**[[NV20|CodeNames]]** || | VPE  | VP1  | VP2  | VP3/VP4/VP5
NV20|| |  |  |  | 
NV25|| |  |  |  | 
NV28|| |  |  |  | 
NV2A|| |  |  |  | 
**[[NV30|CodeNames]]** || | VPE  | VP1  | VP2  | VP3/VP4/VP5
NV30|| | YES (v1) |  |  | 
NV31|| | YES (v2.0) |  |  | 
NV34|| | YES (v2.0) |  |  | 
NV35|| | YES (v1) |  |  | 
NV36|| | YES (v2.0) |  |  | 
**[[NV40|CodeNames]]** || | VPE  | VP1  | VP2  | VP3/VP4/VP5
NV40|| | YES (v2.1) |  |  | 
NV41|| | YES (v2.1) | YES (VP1.0) |  | 
NV42|| | YES (v2.1) | YES (VP1.0) |  | 
NV43|| | YES (v2.1) | YES (VP1.0) |  | 
NV44|| | YES (v2.2) | YES (VP1.1) |  | 
NV46|| | YES (v2.2) | YES (VP1.1) |  | 
NV47|| | YES (v2.2) | YES (VP1.1) |  | 
NV49|| | YES (v2.2) | YES (VP1.1) |  | 
NV4A|| | YES (v2.2) | YES (VP1.1) |  | 
NV4B|| | YES (v2.2) | YES (VP1.1) |  | 
NV4C|| | YES (v2.2) | YES (VP1.1) |  | 
NV4E|| | YES (v2.2) | YES (VP1.1) |  | 
NV63|| |  |  |  | 
NV67|| | ? | ? |  | 
NV68|| | ? | ? |  | 
**[[NV50|CodeNames]]** || | VPE  | VP1  | VP2  | VP3/VP4/VP5
NV50|| | YES (v2.3) | YES (VP1.2) |  | 
NV84|| | YES (v2.3) |  | YES | 
NV86|| | YES (v2.3) |  | YES | 
NV92|| | YES (v2.3) |  | YES | 
NV94|| | YES (v2.3) |  | YES | 
NV96|| | YES (v2.3) |  | YES | 
NV98|| |  |  |  | YES (VP3)
NVA0|| | YES (v2.3) |  | YES | 
NVA3|| |  |  |  | YES (VP4.0)
NVA5|| |  |  |  | YES (VP4.0)
NVA8|| |  |  |  | YES (VP4.0)
NVAA|| |  |  |  | YES (VP3)
NVAC|| |  |  |  | YES (VP3)
NVAF|| |  |  |  | YES (VP4.1)
**[[NVC0|CodeNames]]** || | VPE  | VP1  | VP2  | VP3/VP4/VP5
NVC0|| |  |  |  | YES (VP4.2)
NVC1|| |  |  |  | YES (VP4.2)
NVC3|| |  |  |  | YES (VP4.2)
NVC4|| |  |  |  | YES (VP4.2)
NVC8|| |  |  |  | YES (VP4.2)
NVCE|| |  |  |  | YES (VP4.2)
NVCF|| |  |  |  | YES (VP4.2)
NVD9|| |  |  |  | YES (VP5)
NVD7|| |  |  |  | YES (VP5)
**[[NVE4|CodeNames]]** || | VPE  | VP1  | VP2  | VP3/VP4/VP5
NVE4|| |  |  |  | YES (VP5)
NVE7|| |  |  |  | YES (VP5)
NVE6|| |  |  |  | YES (VP5)
NVF0|| |  |  |  | YES (VP5)
"""]]

Features supported by each hardware generation: 

| hardware | MPEG1 | MPEG2 Main Profile | H.264 High Profile | VC-1 (all profiles) | MPEG 4 Advanced Simple Profile without GMC | other |
|-|
VPE v1 | MC, MC+IDCT [custom data format] | MC, MC+IDCT, MC+IDCT+IQ [custom data format] | none |||||
VPE v2 | MC, MC+IDCT [DXVA format] || none |||||
VP1 | MC+? ||||| fully programmable vector processor |
VP2 | MC, MC+IDCT, MC+IDCT+IQ [DXVA format] || MC+IDCT+IQ [DXVA format], deblock [DXVA format], full bitstream decode | MC, MC+IDCT, MC+IDCT+IQ [DXVA format] || fully programmable vector processor |
VP3 | full bitstream decode |||| MC+IDCT+IQ [custom data format] | none |
VP4 | full bitstream decode ||||| none |
VP5 | full bitstream decode ||||| none |
[hw support]

The minor versions signify hardware changes not introducing any actual new functionality, like redesigning the memory access interface to match the rest of the GPU, etc. 

And now status: 

* **VPE v1**: already REd, not yet supported 
* **VPE v2**: REd and supported by nouveau, though not on all possible cards yet 
* **VP1**: early RE work in progress - watch it unfold on the [[RE blog|https://0x04.net/~mwk/reblog]]. Microcode in an unknown ISA involved. 
* **VP2** H.264 bitstream engine: advanced RE work in progress, about 3/4 done. Microcode in 2 different ISAs involved. 
* **VP2** video processing engine: advanced RE work in progress, aboult half done. Microcode in 3 different ISAs is involved, 1 of them still unknown.
* **VP3**: RE work in progress, about half done. Microcode in 3 different ISAs involved, 1 of them semi-unknown 
* **VP4**: is just VP3 with MPEG4 support, same status 
* **VP5**: RE work not started. Seems to be the same design as VP3/VP4, but with the major video processor part replaced by something new 
