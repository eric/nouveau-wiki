XvMC is an API that exposes hardware decoding capabilities to video players. Nouveau provides some support for this via mesa's XvMC state tracker. For more information, see [[VideoAcceleration]].
