[[!table header="no" class="mointable" data="""
 [[Home|FrontPage]]  |  [[TiNDC 2006|IrcChatLogs]]  |  [[TiNDC 2007|IrcChatLogs]]  |  [[TiNDC 2008|IrcChatLogs]]  |  [[Old Logs|IrcChatLogs]]  |  [[Current Logs|IrcChatLogs]]  | DE/[[EN|Nouveau_Companion_33]]/[[ES|Nouveau_Companion_33-es]]/[[FR|Nouveau_Companion_33-fr]]/RU/[[Team|Translation_Team]] 
"""]]


## Informe irregular sobre el desarrollo de Nouveau


## Edición del 12 de enero


### Introducción

¡Feliz año a todos nuestros lectores!. Bienvenidos de nuevo al primer número del TiNDC de este año. 

Aprovechamos esta oportunidad para agradecer sus esfuerzos durante el 2007 a todas las personas que han hecho aportaciones de código y han realizado pruebas. Esperamos poder ver a algunas de ellas este año en el FOSDEM. 

El último número fue un poco apresurado debido a las vacaciones y al trabajo, por lo que se coló en el wiki cuando probablemente nadie estaba pendiente. 

En el IRC surgió la pregunta de si nouveau debería funcionar correctamente en un sistema NetBSD, a lo que se respondió que "no". Un usuario de netbsd  nos hizo referencia a [[http://mail-index.netbsd.org/tech-x11/2007/04/25/0000.html|http://mail-index.netbsd.org/tech-x11/2007/04/25/0000.html]] , que es algo que ignorábamos completamente. Así que, si alguno de los desarrolladores de NetBSD nos leen en los TiNDC: estamos _muy_ interesados en parches para *BSD, y los aplicaremos gustosos. Así que si existe algo más reciente, por favor avisad :) 

JussiP arregló la página de volcados, por lo que ya es posible ver otra vez el estado de los volcados :) 


### Estado actual

Sobre el asunto de la falta de funcionamiento en la plataforma PPC: Marcheu encontró algunos fallos en el DRM relacionados con PPC y los solucionó ( [[http://gitweb.freedesktop.org/?p=mesa/drm.git;a=commit;h=cd19dcef4f7cc454f68618a0a1e903f159db21ad|http://gitweb.freedesktop.org/?p=mesa/drm.git;a=commit;h=cd19dcef4f7cc454f68618a0a1e903f159db21ad]] ). Aparte de mala programación por nuestra parte, tuvimos el problema adicional de que el acceso a la BIOS tras el inicio no funcionaba correctamente, y la BIOS mostraba signos de corrupción (que no deberian producirse, ya que obviamente llegaba a iniciarse). malc0 se tropezó con este asunto el año pasado y solucionamos el problema copiando la BIOS a la RAM y usando esa copia para trabajar con ella (para interpretar el DCB, etc).  [[http://gitweb.freedesktop.org/?p=mesa/drm.git;a=commit;h=de522ae742bd058780135eb21fe287e9a9dc263a|http://gitweb.freedesktop.org/?p=mesa/drm.git;a=commit;h=de522ae742bd058780135eb21fe287e9a9dc263a]] 

Sin embargo, todavía nos llegaban informes de fallo, que mostraban que se producía ese mismo fallo ocasionalmente. Marcheu optó por realizar la copia lo antes posible en la inicialización del driver, esperando que eso solucionase los problemas en PPC. 

AndrewR envió una mejora para el control de overlay, que pulió un poco ahuillet ([[http://people.freedesktop.org/~ahuillet/irclogs/nouveau-2008-01-07.htm#1348|http://people.freedesktop.org/~ahuillet/irclogs/nouveau-2008-01-07.htm#1348]]).  Debería proporcionar el control de brillo y color de Xv y otras mejoras para las tarjetas NV04/NV05. 

En relación a Xv, marcheu introdujo otras mejoras para sincronizar la copia a la salida de vídeo para evitar algunos problemas de visualización (tearing) en las tarjetas NV40. 

Stillunknwon acabó cogiendo el toro por los cuernos y comenzó a trabajar en el soporte de Randr1.2 para las tarjetas más antiguas (NV1x y NV2x). Junto con Ahuillet, solucionó algunos problemas. 

Su trabajo, más algunos parches de malc0, hizo que se lograse durante las vacaciones: 

* Puede funcionar el dual link con dvi, aunque es necesario hacer pruebas (stillunknown y seventhguardian probaron y actualizaron la 7300 go con  LVDS, pero los primeros intentos no lograron resultados satisfactorios). [[!img http://people.freedesktop.org/~hughsient/temp/nouveau-split.jpg] 
* Tras un par de días de intentos, hughsie reapareció informando sobre el mismo fallo (ve imagen) en un sistema parecido, por lo que LVDS estaba oficialmente roto (¡eh, mucho mejor que la situación anterior de 'no soportado'!). Seventhguardian, con la ayuda de stillunknown, logró resolver finalmente su problema, realizando comabios en nv_crtc.c y mediante prueba y error. 
* Se solucionaron los problemas de establecimiento de modo con Randr1.2 para la 7300 go. 
Además, stillunknown probó suerte con el vídeo texturado (con shaders) para la reproducción de vídeo en NV4x. El sistema de copia (blitter) que hizo ahuillet funciona, pero es un poco lento, por lo que todavía hay margen de mejora si usamos shaders. 

Stillunknown logró que darktama y thunderbird le explicasen algunas cuestiones básicas, y después de varios intentos, logró hacer funcionar un programa de prueba sencillo. Un par de días más tarde, consiguió que funcionase un adaptador yv12 en escala de grises. El trabajo posterior se hizo más lento al encontrarse stillunknown con algunos problemas relacionados con la correcta comprensión de las intrucciones del código de sombreado (shader). Publicó sus resultados del momento para su revisión, que llevaron a cabo Thunderbird y marcheu. 

Una vez solucionadas algunas cuestiones en el código de sombreado, de acuerdo a los comentarios que obtuvo, logró hacer funcionar, un día más tarde, el color. Sin embargo, todavía había problemas con el filtrado bilineal ([[http://people.freedesktop.org/~marcheu/irclogs/nouveau-2007-12-29#1217|http://people.freedesktop.org/~marcheu/irclogs/nouveau-2007-12-29#1217]]) 

Más adelante, ahuillet hizo algunas mejoras en la calidad de imagen del sistema de copia (blitter): interpolación lineal para la conversión YV12->YUY2 ([[http://people.freedesktop.org/~ahuillet/irclogs/nouveau-2008-01-04.htm#2134|http://people.freedesktop.org/~ahuillet/irclogs/nouveau-2008-01-04.htm#2134]]) 

Y, ya que estaba en racha, añadió otra mejora a Xv: ahora la superposición funciona un poco mejor en los sistemas de salida doble. Cambia correctamente el CRTC, y pasa al sistema de copia (blitter) sin que se llegue a ver la pantalla azul, en lugar del vídeo. ([[http://people.freedesktop.org/~ahuillet/irclogs/nouveau-2008-01-06.htm#1547|http://people.freedesktop.org/~ahuillet/irclogs/nouveau-2008-01-06.htm#1547]]) 

Temas cortos: 

* Ahuillet se rindió respecto a la [[PictOp|PictOp]] A8+A8 en PPC y la desactivó (bien, por lo menos lo anunció it :) ) 
* AndrewR y fsteinel_ anunciaron la existencia de problemas en TNT2. AndrewR localizó el cambio que producia el fallo. Se trataba de un problema en [[ImageFromCpu|ImageFromCpu]], que cambió de NV05_IMAGE_FROM_CPU a NV_IMAGE_FROM_CPU y logró solucionar el problema. 
* Malc0 arregló algunos problemas en las NV30 que usan AGP, desactivando y reactivando el AGP en el DRM ([[http://people.freedesktop.org/~ahuillet/irclogs/nouveau-2007-12-28.htm#1353|http://people.freedesktop.org/~ahuillet/irclogs/nouveau-2007-12-28.htm#1353]]) Stillunknown pulió y publicó el cambio posteriormente. 
* marcheu está todavía rehaciendo el código de Xv. Proporcionará un único adaptador genérico que cambiará al que sea necesario (por ejemplo, superposición, copia, etc) en marcha. 
* Marcheu escribió un parche de sincronización a vblank para el adapatador de texturas de xv. También hizo otras cosas en el adaptador de texturas (filtrado, optimización, etc) pero no han sido publicadas. 
* ahuillet fue informado por AndrewR (el hombre de las muchas tarjetas :) ) que su NV05 no funcionaba. Ahuillet no le creyó al principio, pero vio las evidencias en los registros que le proporcionó AndrewR. Extrañamente, la NV05 parece usar métodos por software para algunas funcionalidades. Este hecho era nuevo para Ahuillet, y se puso a investigarlo. Un par de días más tarde, darktama localizó el problema. Un cambio anterior había eliminado los bits que indicaban a la tarjeta que manejase los métodos de software, resultando en una tormenta de interrupciones. Tras reponer el bit adecuado, todo volvió a funcionar. 
* jkolb añadió el código de inicialización de contexto de las tarjetas NV86. 
* El trabajo de Marcheu en la plataforma gallium para dar soporte a tarjetas antiguas se ha parado, debido a cuestiones de la vida real. 
(IRC: [[http://people.freedesktop.org/~ahuillet/irclogs/nouveau-2008-01-11.htm#0248|http://people.freedesktop.org/~ahuillet/irclogs/nouveau-2008-01-11.htm#0248]]) 

En los kernel más recientes (>= 2.6.24) los notificadores de fallo de página han desaparecido y, por tanto, MMioTrace ha dejado de funcionar. PQ ha solicitado ayuda en la LKML (lista de correo del kernel linux) ([[http://marc.info/?t=119982207100002&r=1&w=2|http://marc.info/?t=119982207100002&r=1&w=2]]) pero tuvo una respuesta negativa. Así que, por ahora, si se desea usar MMioTrace, no debes usar un kernel >=2.6.24. PQ está pensando en una solución al problema. Mientras, Airlied, benh y otros, acudieron en ayuda de PQ en la LKML, solicitando la reversión del parche o, al menos, una oferta de algo similar. Parece que PQ preparará todo para la inclusión de un reemplazo de la función eliminada en el kernel 2.6.25 y tendrá la ayuda de los gurús del kernel. 

Y , finalmente: se nos informó varias veces de que nouveau seguía funcionando en PPC (sí, ¡funcionando!). Si eres usuario de PPC, por favor, intenta localizar las regresiones, ya que necesitamos que se nos informe lo antes posible. 


### Ayuda necesaria

Los usuarios de NV4x deberían probar si el adaptador de video texturado les funciona o no, e informar a stillunknown. 

Quienes tengan NV04 / NV05 deberían probar la versión del código en git e informar si les funciona. 

Puesto que el código de randr1.2 cambia a  menudo, se deberían hacer pruebas con frecuencia. Señalad las regresiones a malc0 y stillunknown si apareciese alguna. 

Como siempre, echad un vistazo a la página de "Se buscan probadores" para las peticiones que surjan entre nuestros números. [[http://nouveau.freedesktop.org/wiki/TestersWanted|http://nouveau.freedesktop.org/wiki/TestersWanted]] 
