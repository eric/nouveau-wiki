# UbuntuTips


## Natty Narwhal (11.04)

Feel free to edit this page if there's something missing. 

To compile Nouveau DDX from Git, at least these packages are necessary: 
[[!format txt """
libudev-dev xserver-xorg-dev x11proto-fonts-dev x11proto-video-dev x11proto-xf86dri-dev
"""]]
To compile the Gallium 3D driver from Git, these packages are necessary: 
[[!format txt """
xutils-dev x11proto-gl-dev x11proto-dri2-dev libdrm-dev libxext-dev libxdamage-dev libxfixes-dev libexpat1-dev libxt-dev libxmu-dev libxi-dev libegl1-mesa-dev flex bison libxvmc-dev
"""]]
You will also need to pull some packages in from oneiric.  To do this, first add oneiric to your sources (such as by making a file in /etc/apt/sources.list.d or using system software sources selection).  Then, in order to make sure that you don't upgrade wholesale to this unreleased version, pin its priority in /etc/apt/preferences: 


[[!format txt """
# You will need to remove this (or change oneiric to psychedelic or
# whatever Ubuntu 12.04 gets named) when you want to dist-upgrade to
# oneiric; the do-release-upgrade script won't change it for you (or
# know what went wrong)
Explanation: Don't install oneiric packages by default
Package: *
Pin: release a=oneiric
Pin-Priority: 99
"""]]
You might also need to increase a limit in APT to handle multiple releases: 


[[!format txt """
echo 'APT::Cache-Limit "125165824";' > /etc/apt/apt.conf.d/massive 
"""]]
Then, you can pull in selected packages from oneiric only, using the '-t' flag to apt-get: 


[[!format txt """
sudo apt-get -t oneiric install libdrm2 x11proto-gl-dev x11proto-dri2-dev
"""]]
Be sure to install the natty versions first, otherwise dependencies could unnecessarily come from oneiric instead of natty. 

To compile 32-bit on a 64-bit system: 
[[!format txt """
libc6-dev-i386
"""]]
This package may be needed when you get errors about R300 and R600 drivers: 
[[!format txt """
llvm llvm-dev llvm2.8-dev
"""]]
Compiling libdrm needs this package: 
[[!format txt """
dh-autoreconf
"""]]
Most things here also apply to Maverick Meerkat (10.10). 


## Intrepid Ibex (8.10)

To compile latest 2D driver from git repository in Ubuntu 8.10, these dependencies should be installed: 
[[!format txt """
git-core libpthread-stubs0-dev autoconf libtool xorg-dev mesa-common-dev 
"""]]
 And for Gallium 3D driver in addition to those listed above, these packages should be installed: 
[[!format txt """
xutils-dev build-essential
"""]]
 Also, dri2proto headers from git repository are needed for 3D driver: 
[[!format txt """
git clone git://anongit.freedesktop.org/xorg/proto/dri2proto
cd dri2proto; ./autogen.sh
sudo make install
"""]]
 If 3D driver doesn't load this section should be added to the end of /etc/X11/xorg.conf: 
[[!format txt """
Section "DRI"
    mode 0666
EndSection
"""]]
 


## Gutsy Gibbon (development branch) (7.10)

As of 07/Jul/2007 on `2.6.22-7-generic` it is not necessary to comment out `vm_insert_pfn`. 


## Feisty Fawn (7.04)

When making nouveau.ko and drm.ko make will complain when compiling `linux-core/drm_compat.c`. 

It appears that this is because the kernel which shipped with Feisty is not really 2.6.20 but 2.6.20 + some bits from 2.6.21 including `vm_insert_pfn`, the drm needs `vm_insert_pfn` so when not present supplies its own copy, Ubuntu already supplies `vm_insert_pfn` but advertises itself as 2.6.20 conflicting with the drm build assumptions. 

The end result of this is the Ubuntu kernel headers already have a definition for `vm_insert_pfn` and this drm supplied function in `linux-core/drm_compat.c` can be commented out to read something like; 


[[!format txt """
/*static int vm_insert_pfn(struct vm_area_struct *vma, unsigned long addr,
          unsigned long pfn)
{
    int ret;
    if (!drm_pte_is_clear(vma, addr))
        return -EBUSY;
    ret = io_remap_pfn_range(vma, addr, pfn, PAGE_SIZE, vma->vm_page_prot);
    return ret;
}*/
"""]]
Barring other problems make should complete. 

As of 28/Apr/2007 works for me on `2.6.20-15-generic`. 


## Unresolved / Undefined Symbols

If you are experiencing problems building nouveau, make sure that anything matching nvidia-glx is _completely_ removed.  Open Synaptic and make sure that anything related to nVidia isn't laying around in Residual Config. 


## Resources

[[HOWTO: Nouveau - Open source 3d drivers for nvidia graphic cards|http://ubuntuforums.org/showthread.php?p=6592005]] 
