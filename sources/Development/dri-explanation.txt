--- Definitions

People frequently get confused by the proliferation of acronyms surrounding
OpenGL support on Linux.  This document is a vain hope to reduce the
confusion.  Definitions are given in expository order rather than alphabetical;
hopefully by the end you will understand better.

Hey, I can dream.

I'm focusing on Linux 2.6.

OpenGL:  3D graphics specification from SGI, descended from an earlier product
called IRIS GL, competitor of Direct3D and QuickDraw3D (deceased).  Frequently
abbreviated "GL", particularly on Linux, since "OpenGL" is a trademark, and
use of the trademark requires passing a (non-free) conformance suite.

GLX:  GL is a graphics language, not a windowing system.  "GLX" refers to the
extension that binds GL to the X11 window system, enabling 3D drawing on X
windows.  GLX is an extension to the X protocol.  There are similar bindings
for other window systems too - WGL on Windows, AGL on MacOS.  In X, the client
side GLX library is called libGL, and the server side extension is called
libglx.

Direct rendering:  There are two ways a GL program can get its drawing done.
Either the client can do the drawing itself, or it can pass the GL requests
to the server and have the server do the drawing.  Server-side rendering is
called "indirect rendering", and client-side rendering is called "direct
rendering".  Direct rendering is usually faster.  This is especially true
for Xorg, where the server's indirect renderer is entirely done in software.

DRI:  The Direct Rendering Infrastructure is a technology that enables
direct rendering for GL programs on Linux and BSD operating systems.

Mesa:  Mesa is a work-alike implementation of GL written by Brian Paul and
dozens of contributors.  It contains a software rasterizer, a GL state machine,
and bindings to several window systems including X and Win32.  All the open-
source DRI drivers are based on Mesa, as well as several closed-source DRI
drivers.

DRI driver:  This is where all the excitement happens.  The DRI driver is
responsible for programming the 3D hardware.  Usually DRI drivers use the
Mesa state machine.  In the DRI, the GLX client-side library loads a DRI
driver, named something_dri.so.

DRM driver:  This is the kernel-side component of the DRI.  The DRM is
responsible for security and handling resource contention.  Not particularly
interesting, but mandatory.  These are named something.ko in Linux 2.6.

DDX driver:  This is a part of the X server, responsible for the other sorts
of X drawing like Render and Xv.  The DDX has to be DRI-aware, but otherwise
not much GL-related happens in the DDX.

AGP:  AGP is a fast version of the PCI local bus, with additional features
for graphics hardware.  The core AGP kernel module is called agpgart.ko.

AGP chipset:  Different motherboards have different chipsets and thus
different implementations of the AGP feature set.  You need the one
corresponding to your motherboard chipset.  This is probably not always the
same as your video chipset.  Via chipsets need via-agp.ko, etc.

--- Configuration

So what do you need?

- Kernel:
DRM driver corresponding to your video card
AGP core support
AGP chipset support corresponding to your motherboard

For most supported cards, you have the DRM driver already.  The exceptions as
of 2.6.9-ish are mach64 and savage.  For those, you need to build the DRM
from CVS, as outlined on http://dri.freedesktop.org/wiki/Building .

Every kernel since the dawn of time has the necessary AGP bits.

Load the AGP core module.  Load the AGP chipset module.  Load the DRM module.
Make sure /dev/dri/card0 exists and is writable by you.

- Userspace:
DRI driver
DRI-aware DDX
DRI-aware libGL

For most supported cards, you have the userspace pieces already.  The
exceptions, as of Xorg 6.8, are again mach64 and savage.  For those you need to
build the Xorg server with "#define BuildDevelDRIDrivers YES" in host.def.

Put Load "dri" and Load "glx" lines in xorg.conf.  Make sure you're using the
DRI-aware DDX driver and not vesa or fbdev or whatever.  Start X.  Make sure
'grep Direct /var/log/Xorg.0.log' says "Direct rendering enabled".  Run
glxinfo, and check that it says "direct rendering: Yes".  Congrats, you win.

Yes, it really is that easy.
