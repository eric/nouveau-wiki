[[!table header="no" class="mointable" data="""
 [[Home|FrontPage]]  |  [[TiNDC 2006|IrcChatLogs]]  |  [[TiNDC 2007|IrcChatLogs]]  |  [[Old Logs|IrcChatLogs]]  |  [[Current Logs|IrcChatLogs]]  | DE/[[EN|Nouveau_Companion_28]]/[[ES|Nouveau_Companion_28-es]]/[[FR|Nouveau_Companion_28-fr]]/RU/[[Team|Translation_Team]] 
"""]]


## The irregular Nouveau-Development companion


## Issue for October, 14th


### Introduction

Back with issue 28 about nouveau, the free driver project for NVidia cards. 

No, we are not working on our own just for the sake of getting a driver, we do get help and do help other projects too. Haiku on the input side of things and MMioTrace on the output side are some examples and it seems during the last 2 week we saw the birth of just another one: Support for PS3 graphics. 

It all started when [[IronPeter|IronPeter]] came into our channel and asked whether we knew anything about PS3 development and how to address NVidia cards there. We gave him some pointers (but had to admit that we couldn't help much with PS3 specific things as none of our developers has a PS3) and during the next days he came back asking for more information about how Nvidia cards work which we happily supplied. 

After some work he finally got simple 2D blits working on PS3, avoiding at least one severe limitation of Linux on PS3. But see for yourself. All-important work was done by [[IronPeter|IronPeter]], we only gave him some technical hints. 

Reading this issue you'll probably notice that not much is written about the 3D part of the driver. That is mostly due to the fact that we need to bite the bullet and use TTM (which should be as good as "finished" as mentioned in the last issue). And a second "problem" is the new driver architecture "Gallium3D". 

It depends heavily on newer features (like vertex programs, shaders etc.) and tries to take away much burden from the driver writer and put supporting infrastructure into Mesa. Of course old drivers and new drivers are not very similar. Gallium3D is bleeding edge and may as yet see a few revisions until it gets a stable API. And additionally, Gallium3D depends on those new card features. Writing a driver for older cards isn't that easy. 

So we are once again waiting. This time however we have a plan: Get 2D working for all cards up to NV4x and perhaps do a first "release". The release would have to be taken with a grain of salt though, as the binary API may still be broken after that. 


### The Current Status

Back to our core project where we saw quite some progress too. Currently we are aiming for a working 2D driver (including EXA) for all models from NV04 to NV4x. Unfortunately NV5x is a different beast and may take more time. Still, until 05.10.2007 our master branch didn't work at all on NV5x / G8x but after some tests and bisecting done by KoalaBR, stillunknown found and fixed an error leading to a working 2D. Annoyingly, switching back to text mode leaves the display dead which makes testing the driver in case of errors hard. 

On NV04 ahuillet fought an uphill battle to get X working. His first tries were met with ignorance from the card; the card simply didn't react at all. Just when ahuillet wanted to give up, matc showed him that he had made some faulty assumptions. Adding to that was a hardware problem with the mainboard, which resulted in random hardware init failures with both a NV04 and a NV11. 

So ahuillet scrapped his code, restarted tried and finally got progress: 

His software methods were called by the card, invalid commands generated the expected error interrupts. Next thing is the implementation of a few software methods. Software methods are commands that, when sent to the card, make the card ask the DRM to handle them. NV04 implements a few initialization related methods in software, maybe as a result of some design decision. Those methods merely set a flag in object instances, and are therefore not hard to write. When software methods need to be handled, the card generates a specific interrupt. 

He got his code in his private git repository and expects it to be pushed to the master repository "soon" (next days). First, ahuillet needs to resolve one last issue, though X started but is slow as molasses as the card generates an interrupt storm of STATE_INVALID. It may have something to do with how we handle interrupts (there was a small change by ahuillet and darktama) but more like it is something else (maybe the state needs to be acknowledged). Investigation is proceeding. And before we forget: Overlay on NV04 works too. 

pmdata tried to kill the rendering problems with glxgears with non default sized windows. Due to other work he had to stall that effort for now. 

pq was trying to get NV20-demo working. He used MMio to get a trace and began analyzing it. To ease his debugging pains he started writing a mmio-trace replayer. This tool take a MMioTrace dump and replays it line by line. So you can see what register writes have what kind of effect. That and some trial and error reduced the log size from 60MB down to 4500 lines. That was no small feat because every replay of the traces needed a reboot. But as the card state survives even a cold boot for power offs shorter than 30 seconds, that endeavor took quite some time. In the end Marcheu remembered the reason while the problem occurred: There was no PGRAPH context init in the DRM at all, no one had written one yet. 

After some testing he got initialization working and rendering of triangles too (within NV20_demo). Next step was getting glxgears working. As a quick hack, pq patched MESA to think that his NV28 was an NV1x card, which worked within the same limits as NV1x (more about that above and in the previous issue). The patch was never committed though as it is a hack and needs proper implementation in the driver. 

However, init means correct per context init and that in turn needed larger structural changes in the DRM, breakage for all NV2x cards except NV28/NV25 was the result. At that time malc0_ offered his patch set to clean up init for NV2x quite a bit. This got all NV2x cards working again. Another result of all patches combined is that NV2x cards do context switching automatically and don't need manual interception anymore. You can find more info about NV2x context init here. 

Work on NV20 proved to be difficult as pq always got a DMA queue hang very early in the X startup process. 

When discussing his problems with marcheu, pq was asked to force notifiers not to be stored in AGP memory but in the memory of the card. That worked and proved that pq has some AGP problems: Notifiers placed in that memory region don't work (that is true for certain motherboards). (For an explanation of notifiers please refer to the last two TiNDCs). Marcheu and JKolb did work hard to get NV3x working. First they tried to get NV30_demo working (that is rendering colored, bare and textured triangles). That proved difficult due to some init errors. After copying the init code 1:1 from nouveau it worked somewhat but crashed only after a few FIFO commands executed with INVALID_COMMAND. 

Quite a few bugs were found and squashed and jkolb reached the next goal: A triangle was rendered. But always black (and as the background was black too, first diagnosis was:" It doesn't render at all :)). Next step done by marcheu: A colored triangle. Not to be outdone jkolb added texturing to the mix. Next goal was then to get EXA working on NV30. Again marcheu did it again with help from jkolb but when marcheu tested desktop performance with a composite manager and compared performance with "[[MigrationHeuristics|MigrationHeuristics]] Always" (should be fastest with EXA) against "[[MigrationHeuristics|MigrationHeuristics]] Greedy" (fastest when EXA is not fully accelerated). So EXA on NV3x is currently not yet usable, due to performance and due to some missing routines (plus some bugs). 

Some short topics: 

* Some memcpy() routines were inlined and gave some noticeable speed improvements. 
* Once again some PPC endian bugs has been under investigation by Marcheu. Some proved difficult to track but with the help of BenH, marcheu was able to commit a fix. Unfortunately the reporting PowerBook user had a hard disk crash, so that we are not sure yet whether the fix works. Even worse, it seems a x86/x64_86 only compile time option has crept into the makefile (for inlining see above). 
* In order to make the NV5x driver more maintainable GrowlZ took on the task to deobfuscate the driver which we "stole" from X11's nv driver. It was only slightly deobfuscated by darktama then mostly in parts where it got adapted to our DRM infrastructure. 
* pq did some work on creating docs from his rules-ng database too. Find the results here. When ahuillet saw that, he promised to write up register docs for Xv too. 
So what is missing now for a acceptable 2D driver? 

* NV10 EXA needs to be implemented 
* NV04 needs to calm that IRQ storm 
* NV20 will use NV10 EXA  
* NV30 EXA needs to get faster (with still some fallbacks, which should be accelerated not software) 
* NV40 EXA is working but not as fast as it could 
* NV50 text mode setting for a very basic driver (no Xv yet) 

### Help Needed

Ahuillet is asking for testers of his NV04 code. And if someone is willing to write some documentation, please contact him too. He will give you any info you'll need, but he currently hasn't the time to do it himself (a good newbie task). 

If you have a PPC based system, please do test and report back to marcheu. 


### Thank You

Once again, we were the target of a hardware donation. So, on behalf of the Nouveau project Pekka Paalanen (pq) would like to thank Ville Herva from Vianova Systems Finland Oy for donating a GeForce 3 card and a GeForce 256 card. Thank you very much! 

[[<<< Previous Issue|Nouveau_Companion_27]] | [[Next Issue >>>|Nouveau_Companion_29]] 
