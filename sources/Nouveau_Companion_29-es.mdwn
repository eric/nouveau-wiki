[[!table header="no" class="mointable" data="""
 [[Home|FrontPage]]  |  [[TiNDC 2006|IrcChatLogs]]  |  [[TiNDC 2007|IrcChatLogs]]  |  [[Old Logs|IrcChatLogs]]  |  [[Current Logs|IrcChatLogs]]  | DE/[[EN|Nouveau_Companion_29]]/[[ES|Nouveau_Companion_29-es]]/[[FR|Nouveau_Companion_29-fr]]/RU/[[Team|Translation_Team]] 
"""]]


## Informe irregular sobre el desarrollo de Nouveau


## Edición del 28 de octubre


### Introducción

Aquí estamos de nuevo. Preparaos para el número 29 del TiNDC. Esta versión está ligeramente actualizada respecto a la publicada en Phoronix, ya que olvidé una cuestión importante. 

Cuando hablábamos en anteriores números sobre las infraestructuras del proyecto, casi siempre lo era en términos negativos (no funcionaban los scripts, era necesario trasladarse a fd.o, etc). Finalmente, podemos informar positivamente sobre ello: Tenemos versiones HTML, con enlaces y apodos coloreados, gracias a un script hecho por MJules. Su versión fue bien recibida e incluso permitía la creación de marcas, de forma que se podía crear un enlace a una determinada línea del registro (usando '==== (tema)'). Se pueden ver los temas señalados aquí: [[http://people.freedesktop.org/~ahuillet/irclogs/tags_index.htm|http://people.freedesktop.org/~ahuillet/irclogs/tags_index.htm]] 

Pero el script proporcionaba algunos falsos positivos, y pachi se puso a mejorarlo. El script es ahora capaz de añadir anclas HTML automáticamente en base a las marcas de hora. Se pueden encontrar tanto el script como las hojas de estilo, y los registros coloreados en [[http://people.freedesktop.org/~ahuillet/irclogs|http://people.freedesktop.org/~ahuillet/irclogs]] . 

Estas nuevas opciones van a ser muy prácticas para el TiNDC. Decidnos si os gusta como las usamos o no. 

Además, ahora tenemos también una página a la que se anuncian las peticiones más importantes de pruebas. Así que si quieres ayudarnos y no te apetece escribir código, por favor echa un vistazo aquí: [[http://nouveau.freedesktop.org/wiki/TestersWanted|http://nouveau.freedesktop.org/wiki/TestersWanted]] 

Un último asunto antes de empezar con el informe de los avances hechos: surgió la pregunta de por qué existe tanto desfase entre la publicación del TiNDC (N. del T.: solamente para la versión original en inglés) en Phoronix y su publicación en el wiki. Es sencillo: 

* Un breve periodo de gracia de unas pocas horas 
* La existencia de distintas zonas horarias (que encaja bien con la primera razón) 
* Y, en esta ocasión, por motivos debidos a la vida 'real', ya que no pude acceder a mi ordenador antes del medio día del Sábado (CEST). A esa hora KMeyer ya había desaparecido. 

### Estado actual

ahuillet añadió un parche para mejorar el rendimiento en las NV04. El parche añade una caché de llamadas a BLIT_OPERATION y RECT_OPERATION. Ya que estos son métodos por software, las llamadas frecuentes a los mismos producen a menudo un enlentecimiento notable del sistema. ([[http://gitweb.freedesktop.org/?p=nouveau/xf86-video-nouveau.git;a=commit;h=6d8caf5e0dd915809152c52c2c56a39d76e2ed8c|http://gitweb.freedesktop.org/?p=nouveau/xf86-video-nouveau.git;a=commit;h=6d8caf5e0dd915809152c52c2c56a39d76e2ed8c]]) 

Desgraciadamente, las pruebas posteriores mostraron que el parche hacía fallar las operaciones de dibujo 2D si una aplicación SDL modificaba los modos de visualización. 

Al final del último número vimos como ahuillet, pmdata y p0g se batían con las dificultades del coloreado y texturado de triángulos y cuadrángulos en NV1x. El resultado variaba tremendamente (desde el cambio de colores sobre un fondo cuadrado en negro a la representación parcial de texturas (1 Texel), se puede elegir el fallo que más nos guste y nuestros desarrolladores pueden contarte que ya han pasado por él). 

Sin embargo, al final lo lograron, y se reducía a la ausencia de un bit para indicar el texturado en el código de inicialización. 

P0g trabajó más sobre la mezcla / transparencia en NV1x. Los primeros intentos obtenían bordes negros en torno a los iconos en lugar del fondo. Tras investigar algo más detectó al culpable: la transparencia no estaba activada, por lo que acabó cocinando un parche: [[!img http://peter.whoei.org/nouveau/icons-semi-working.png] 

(IRC: [[http://people.freedesktop.org/~ahuillet/irclogs/nouveau-2007-10-23#0022|http://people.freedesktop.org/~ahuillet/irclogs/nouveau-2007-10-23#0022]]): [[http://cgit.freedesktop.org/nouveau/xf86-video-nouveau/commit/?id=cf053adacabaee887ecedaa9967b07b3185095b5|http://cgit.freedesktop.org/nouveau/xf86-video-nouveau/commit/?id=cf053adacabaee887ecedaa9967b07b3185095b5]] 

Otro problema solucionado era el de las texturas con dimensiones que no eran potencias de dos necesitaban tener tamaños pares. Se redondearon hacia arriba los tamaños impares. 

Aunque estos resultados son fundamentales para hacer funcionar EXA en las NV1x (y se rumorea que el texturado será de gran utilidad luego para 3D), todavía faltan algunas piezas importantes: [[PictOps|PictOps]] y las máscaras. 

Justo antes de acabar el periodo de edición del TiNDC, ahuillet hizo funcionar las máscaras, y también algunas [[PictOps|PictOps]]. Ya que NV1x no tiene funciones de sombreado, utilizó sábiamente los registros de combinación (ver inciso). Sin embargo, la importantísima [[PictOpt|PictOpt]] A8 + A8 (Alpha 8 bit) todavía no está implementada. La razón es que NV1x no admite A8 como destino. Hasta que se implemente, EXA en NV1x realmente no parecerá acelerada. [[http://people.freedesktop.org/~ahuillet/irclogs/nouveau-2007-10-25.htm#1545|http://people.freedesktop.org/~ahuillet/irclogs/nouveau-2007-10-25.htm#1545]] 

Por todo ello ahuillet aclamaba victoria sobre el equipo de marcheu y jkolb en la carrera para ver qué tarjeta tendría EXA en funcionamiento antes: NV1x (ahuillet, p0g) o NV3x (jkolb, marcheu). 

Para su desgracia, marcheu advirtió un par de horas más tarde que EXA en NV1x no es capaz de manejar XBGR, que, de acuerdo a sus números, significaría que la mayoría de las llamadas a EXA acabarían en funciones de soporte por software. ([[http://people.freedesktop.org/~marcheu/irclogs/nouveau-2007-10-26#0247|http://people.freedesktop.org/~marcheu/irclogs/nouveau-2007-10-26#0247]]) 

Así, otro par de horas más tarde, aplicó un parche para EXA en NV3x que resolvía todos los temas pendientes. A8 op A8 se arregló más adelante con una actualización del DRM y ahora funciona, así como también la repetición de áreas de dimensiones 1x1. Lo que falta para un soporte completo de EXA es la repetición de áreas mayores a 1x1, pero no se usa demasiado y requerían el uso de una función de sombreado para calcular el envoltorio de coordenadas necesario. Por desgracia, las auténticas NV30 no funcionan en estos momentos ya que la inicialización en el DRM parece ser incorrecto (esto sería para las GeForce 5800, Quadro FX1000). [[http://people.freedesktop.org/~marcheu/irclogs/nouveau-2007-10-26#0451|http://people.freedesktop.org/~marcheu/irclogs/nouveau-2007-10-26#0451]]) 

**Inciso: Combinadores de registros** 

---

 Ya que las NV1x carecen de funciones de sombreado de ningún tipo es necesario el uso de combinadores de registros (o texturas) para obtener los mismos resultados que las tarjetas más modernas logran con ellas. 

Aunque los combinadores de registros y de texturas funcionan de forma distinta en las operaciones 3D, pueden verse como casi iguales, si no idénticos, a efectos de nuestro trabajo actual en 2D. Los combinadores de registros están incrustados en ta etapa de texturado del hardware, por lo que, cuando se configura esa parte de la tarjeta, se inicializan en el funcionamiento predeterminado. 

Ahora bien, ¿qué se obtiene de forma predeterminada?. Si se muestra un pixel en la pantalla, éste se modula (texel modulation) mediante los colores del vértice. Si se quiere modular con cualquier otra cosa (por ejemplo el alpha u otra textura), simplemente se establecen de acuerdo a ello los combinadores de registros que implementarían la operación IN, que es necesaria para la aplicación de la máscara. 

---

 

Después de que ahuillet anunciase un funcionamiento (casi) completo de EXA para NV1x, pq empezó a probarlo en sus tarjetas NV2x. Poco después de hacer las pruebas, descubrió que [[DownloadFromScreen|DownloadFromScreen]] (abreviado como DFS) no funcionaba. Biseccionó el problema hasta llegar a la última ruptura de la api y todavía en ese punto no funcionaba correctamente DFS. 

jkolb y marcheu prosiguieron trabajando en la NV30. Se resolvieron algunos problemas: 

* El posicionamiento en EXA, así como el tamaño de los rectánsulos sufría desplazamientos. 
* Las coordenadas en EXA no se calculaban correctamente. Se transformaban para que encajasen en un intervalo [0..1] en lugar de [0..width]. Jkolb lo solucionó. 
En ese punto el driver no era capaz de manejar la transparencia, por ejemplo en iconos. Todas las partes transparentes se dibujaban en negro. Jkolb probó suerte y fue vencido por la tozudez de la tarjeta, mientras que marcheu probó suerte y la tuvo<sup>W</sup>W mostró su profesionalidad: 

En resúmen, NV10 está ahora muy cerca de alcanzar a las NV3x. Les falta la [[PictOp|PictOp]] A8+A8. 

Aunque darktama ha estado muy tranquilo las últimas semanas no ha sido porque haya estado vagueando. Trabajó algo en un prototipo para las NV4x de TTM y Gallium (simplemente para investigar, ya que el código no será publicado tal cual está). Tras comentar con marcheu los pros y los contras de la arquitectura ambos llegaron a la conclusión de que Gallium es el camino a seguir. 

Mientras marcheu y darktama llegaban a una decisión, pmdata quería iniciar el trabajo en las funciones DRI para NV1x, pero marcheu le pidió que no lo hiciese por las siguientes razones: 

* En estos momentos queremos que EXA funcione y, con todos los avances ya mencionados, eso no llevará mucho más tiempo. 
* Gallium es distinto al DRI, especialmente para las tarjetas NV2x y anteriores, y sería una pérdida de tiempo dedicarse al DRI. 
* Marcheu tiene confianza en que las tarjetas >=NV3x no tendrán demasiados problemas en tener un driver Gallium, pero las tarjetas anteriores necesitarán algunos apaños (que supermarcheu va a proporcionar :) ) 
Ver: [[http://people.freedesktop.org/~ahuillet/irclogs/nouveau-2007-10-19.htm#2025|http://people.freedesktop.org/~ahuillet/irclogs/nouveau-2007-10-19.htm#2025]] 

Con esto en mente, marcheu dejó aparcados todos sus otros proyectos en nouveau para montar una infraestructura genérica en Gallium que permita a las tarjetas más antiguas que no disponen de lineas de procesado (pipelines) o que disponen de lineas de procesado fijas ofrecer algún grado de aceleración. En el caso de Nouveau estaríamos hablando de todas las tarjetas anteriores a la NV30. 

Puesto que este trabajo es genéricos, beneficiará a todas las tarjetas que carecen de líneas de procesado de vértices y sombreado (por ejemplo, las radeon antiguas). Tras ello pmdata podrá comenzar a trabajar en el DRI. 

Stillunknown fue perseguido por KoalaBR hasta que se rindió y se ofreció a intentar hacer funcionar el establecimiento de modo en las NV50 / G84. En un acto de revancha nombró a KoalaBR como probador. Los primeros intentos tímido de parchear el establecmiento de modo para hacerlo funcionar no tuvieron éxito. Por lo que todavía no podemos volver al modo de texto desde las X11. 

Otros asuntos que merece la pena mencionar: 

* El parche de documentación para rules-ng de hkBst sobre los registros relacionados con Xv fueron integrados por pq 
* [[IronPeter|IronPeter]] ha avanzado mucho con su trabajo en la PS3. 
* Debido al problema del que hablamos en el número anterior, ahora los notificadores se crean siempre en la memoria de PCI. 
* stillunknown añadió algunos registros de control de video para NV50 a rules-ng. 
* pmdata está trabajando en descubrir los formatos de texturas soportados por las NV1x y ayudó a ahuillet y p0g con los probelmas de las NV1x 
* Se arregló un problema con el DRM que permite evitar que la tarjeta se cuelgue en caso de error. 
* careym arregló un par de problemas de OpenGL en renouveau 
* chowmeined solucionó algunos problemas de establecimiento de modo en NV5x que producían líneas rosas en el lateral derecho e inferior de la pantalla. 
El trazado Mmio en tarjetas nv50 puede colgar la máquina. KoalaBR lo descubrió (aunque no lo pudo reproducir más tarde), y pq lo puede reproducir y trata de investigar el problema, aunque le gustaría que alguien con el hardware apropiado pudiese reproducir el cuelgue y trabajase en ello con una terminal en puerto serie (usando otro ordenador). La tarea no requiere demasiado conocimiento de programación, pero precisa de la habilidad de configurar e instalar un kernel personalizado, aplicar algunos parches, y tener paciencia con los cuelgues y reinicios que se producirán de vez en cuando. También es probable que se trate de una tarea que llevará un tiempo, por lo que esa persona debería tener posibilidad de estar disponible durante un par de semanas por lo menos. 


### Ayuda necesaria

stillunknown busca gente que tenga tarjetas 7xx0 y que quiera hacer algunas pruebas para él. Además, está buscando gente con tarjetas de doble salida (en cualquier combinación de VGA/DVI) que estén dispuestas a probar el establecmiento de modo / randr1.2. 

Además, por favor, echad un vistazo a la página de "Se buscan probadores" para los asuntos que surjan entre las ediciones del TiNDC: [[http://nouveau.freedesktop.org/wiki/TestersWanted|http://nouveau.freedesktop.org/wiki/TestersWanted]] 
