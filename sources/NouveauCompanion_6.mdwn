## The irregular Nouveau-Development companion


## Issue for October 27, 2006


### Introduction

_**Flashback to Friday, 27th October**_  
  
 Somewhere in Germany, late afternoon. We see KoalaBR starting up the computer, after coming home from work: 

KoalaBR: _Well, now let's get the new companion online_  
 _Hm, no connection? ... Oh, DSL is off... Strange it is always on_  
 _... No does not work *Gulp*_   
 `Dialing support number`  
 Support: _We will send you your new modem on Monday. It will arrive the next possible day, which would be Tuesday_  
 KoalaBR: _So it will arrive on Tuesday?_  
 Support: _Within 1 or 2 working days, so Tuesday or Wednesday, yes_  
 KoalaBR: _Uh, isn't Wednesday a national holiday?_  
 Support: _Oh, right you are. Well Thursday then, latest_  
 KoalaBR: **Noooooooooo....**  
 

We now return to our program, originally scheduled for Friday, 27th of October. 


### Intro

Hello, as I said in the first TiNDC, issues won't be coming out regularly once a week. It seems to settle into a "about every two weeks" cycle. This is mainly for two reasons: Not much time on my hands currently and little progress regarding nouveau within a week. This time, we had some problems with our IRC logs, which stopped working last weekend until marcheu restored them to a working state. However, until then, I didn't have enough material to create a TiNDC. 

If you have questions or want some special topics covered in the next issue, just post your question(s) to our IRC channel. Even if no one responds immediately, it may show up in our next issue. 


### Current status

Well, it seems as if Lumag has basically left our project. It was intended to get him sponsored via the Google Summer of Code, but it didn't work out. As Lumag then needed to shift his priorities, he had less time for us. Bad luck. Thank you Lumag for your contributions! 

On the programming side there is currently not much to report. As reported last time, we need working framework for context switching. Basically a context is an OpenGL or 2D Window (like e.g. a game or the X server), which are users of the graphic card. To serve more than one user, the card has a command buffer for each user, which holds the commands for the card and some other management data (where can a texture be found, which command was executed last, etc.). So in order to serve more than one user, the card needs to switch between those setups (contexts). 

Well since the last TiNDC Darktama managed to get a context switching working on a NV40+ class card. That means he was able to switch between to contexts which worked for a short time ("short time" for a period of more than one or two interrupts and for more than one or two commands in total). However, other cards still don't work correctly. 

Daktama's work was based on earlier work of marcheu, who did memory management work and tried his luck with context switching (on a NV20 class card). However, he couldn't get it stable, sometimes it would work, sometimes not. 

As Marcheu put it, we are currently very close to a breakthrough. However, this breakthrough may take any time from a few hours to a few weeks. 

Meanwhile, Arlied is working on cleaning up / deobfuscating the nouveau driver, so that it is both better readable and maintainable in the future. This is necessary, as we started of the nv driver from Xorg. This one is heavily obfuscated (constants instead of defines as a really ugly example should be sufficient here). 

After this work is completed, Arlied intends to work on mode setting and switching (from text console to X and vice versa), which may even result in correct dual head working. 

On IRC there was a request seconded by a few lurkers to add a section "Junior Jobs" to the website, currently there is not much to do but to start cleaning up the DDX, but perhaps we will see such a page in the future.  


### Help needed

We always need help, just drop into #nouveau on freenode.org and offer your help. If you have any links to articles / blogs etc. about our project, please let me know! 

[[<<< Previous Issue|NouveauCompanion_5]] | [[Next Issue >>>|NouveauCompanion_7]] 
