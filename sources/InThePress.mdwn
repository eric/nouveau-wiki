## Nouveau in the press

Collected links to articles and stories considering Nouveau, in reverse chronological order. 

* _Oct 22, 2010_ [[http://www.phoronix.com/scan.php?page=news_item&px=ODcwMw|http://www.phoronix.com/scan.php?page=news_item&px=ODcwMw]] Article on introduction of the pageflip IOCTL 
* _Oct 13, 2010_ [[http://www.linux.org.ru/news/hardware/5438270|http://www.linux.org.ru/news/hardware/5438270]] In response to our call for help on memory timing management 
* _Oct 11, 2010_ [[http://www.phoronix.com/scan.php?page=news_item&px=ODY2Nw|http://www.phoronix.com/scan.php?page=news_item&px=ODY2Nw]] In response to our call for help on memory timing management 
* _Sept 15, 2010_ [[http://www.phoronix.com/scan.php?page=news_item&px=ODYwMA|http://www.phoronix.com/scan.php?page=news_item&px=ODYwMA]] First article on Power Management 
* _Feb 11, 2009_ [[http://lxer.com/module/newswire/view/115938/index.html#nouveau|http://lxer.com/module/newswire/view/115938/index.html#nouveau]] Article about Fosdem 2009 nouveau status. 
* _Jul 11, 2008_ [[http://tech.slashdot.org/tech/08/07/11/1210224.shtml|http://tech.slashdot.org/tech/08/07/11/1210224.shtml]] Article about the current work on xvmc & nouveau. 
* _Mar 17, 2008_ [[http://www.heise.de/newsticker/meldung/105144|http://www.heise.de/newsticker/meldung/105144]] The last paragraph of the so called “Kernel Log” mentions Nouveau's state (German) 
   * **Translation of the relevant paragraph:**   
 [...] And also the developers of the open Nvidia driver “Nouveau” are laborious and improve the driver in the developer tree to add support for some of the newest graphic chips from Nvidia. Just recently there was a new nv driver version too, which improved the support of newer graphic chips and cards; while the nv driver supports only the base functionality of the Nvidia graphic chips, the Nouveau driver aims to provide 3D and multi-head support. 
* _Mar 12, 2008_ [[http://hboeck.de/archives/599-A-try-on-current-nouveau.html|http://hboeck.de/archives/599-A-try-on-current-nouveau.html]] Blog with video running openarena on gallium-accelerated nouveau 
* _Feb 28, 2008_ [[http://lwn.net/Articles/270830/|http://lwn.net/Articles/270830/]] LWN article about Nouveau, part 2. 
* _Feb 21, 2008_ [[http://lwn.net/Articles/269558/|http://lwn.net/Articles/269558/]] LWN article about Nouveau, part 1. 
* _Feb 19, 2008_ [[https://linuxfr.org/2008/02/19/23727.html|https://linuxfr.org/2008/02/19/23727.html]] french article about the current state of Nouveau 
* _Sept 4, 2007_ [[http://www.linux.com/feature/118833|http://www.linux.com/feature/118833]] an article about Nouveau at linux.com 
* _Sept 3, 2007_ [[http://www.phoronix.com/scan.php?page=article&item=820&num=1|http://www.phoronix.com/scan.php?page=article&item=820&num=1]] Nouveau Q&A at Phoronix 
* _June 30, 2007_ [[http://www.phoronix.com/scan.php?page=article&item=765&num=1|http://www.phoronix.com/scan.php?page=article&item=765&num=1]] an article about Nouveau with some factual problems (e.g. the screenshot uses Mesa instead DRI accelerated). 
* _June 5, 2007_ [[http://www.abclinuxu.cz/clanky/rozhovory/andy-ritger-nvidia?page=1|http://www.abclinuxu.cz/clanky/rozhovory/andy-ritger-nvidia?page=1]] Andy Ritger from NVidia's Linux Group (among other topics) about Nouveau 
* _Mar 21, 2007_ [[http://digg.com/linux_unix/Nouveau_GSoC_developer_wanted|http://digg.com/linux_unix/Nouveau_GSoC_developer_wanted]] self advertizing 
* _Mar 8, 2007_ [[http://linuxfr.org/2007/03/08/22182.html|http://linuxfr.org/2007/03/08/22182.html]] The official announce in linuxfr. 
* _Mar 7, 2007_ [[http://linuxfr.org/~Nahuel/23935.html|http://linuxfr.org/~Nahuel/23935.html]] blog in French 
* _Mar 6, 2007_ [[http://digg.com/linux_unix/Nouveau_Open_Source_3D_needs_you_pleas_send_dumps_of_your_card|http://digg.com/linux_unix/Nouveau_Open_Source_3D_needs_you_pleas_send_dumps_of_your_card]] And the people started to pour in... 
* _Feb 19, 2007_ [[OpenGL.org|http://www.opengl.org/news/permalink/nouveau_project_to_develop_open_source_opengl_api_acceleration_for_nvidia_c/]] Just a note that we exist 
* _Jan 28, 2007_ [[http://www.phoronix.com/scan.php?page=article&item=634&num=1|http://www.phoronix.com/scan.php?page=article&item=634&num=1]] Phoronix's article - Nouveau: glxgears on NV4x 
* _Jan 22, 2007_ [[http://community.linux.com/article.pl?sid=07/01/18/2249209&tid=53|http://community.linux.com/article.pl?sid=07/01/18/2249209&tid=53]]  A short overview on Airlied's talk at LCA 2007 (near the end) 
* _Jan 16, 2007_ [[http://lwn.net/Articles/217866/|http://lwn.net/Articles/217866/]] LCA: The state of Nouveau project. 
* _Jan 9, 2007_ [[http://linux.slashdot.org/linux/07/01/09/2026252.shtml|http://linux.slashdot.org/linux/07/01/09/2026252.shtml]] It's official, we got slashdotted for the PledgeBank thing. 
* _Dec 25, 2006_ [[http://digg.com/linux_unix/Nouveau_A_First_Look|http://digg.com/linux_unix/Nouveau_A_First_Look]] Digg about the Phoronix article below. 
* _Dec 25, 2006_ [[http://www.osnews.com/story.php/16814/Nouveau-First-Look-at-Open-Source-3D-NVIDIA-Drivers|http://www.osnews.com/story.php/16814/Nouveau-First-Look-at-Open-Source-3D-NVIDIA-Drivers]] OSnews linking to the Phoronix article below. 
* _Dec 25, 2006_ [[http://www.phoronix.com/vr.php?view=8293|http://www.phoronix.com/vr.php?view=8293]] Phoronix's first review on Nouveau. 
* _Dec 22, 2006_ [[http://lwn.net/Articles/215378/|http://lwn.net/Articles/215378/]] LWN item advertizing the pledge, with user comments. 
* _Dec 21, 2006_ [[http://lovesunix.net/blog/?p=153|http://lovesunix.net/blog/?p=153]] David Nielsen about why he created the Nouveau pledge on PledgeBank. 
* _Nov 28, 2006_ [[http://www.linux-gamers.net/modules/news/article.php?storyid=1863|http://www.linux-gamers.net/modules/news/article.php?storyid=1863]] A short note about existence. 
* _Nov 21, 2006_ [[http://openfree.wordpress.com/2006/11/21/3d-nvidia-linux-driver-effort-new-milestone/|http://openfree.wordpress.com/2006/11/21/3d-nvidia-linux-driver-effort-new-milestone/]] A blog entry about the PledgeBank case, which was **not** started by the Nouveau developers. 
* _Nov 14, 2006_ [[http://www.linux.com/article.pl?sid=06/11/13/2112259|http://www.linux.com/article.pl?sid=06/11/13/2112259]] Ubuntu Developer Summit report: X.org improvements, driver controversy, and bling 
* _Nov 11, 2006_ [[http://digg.com/linux_unix/3D_Nvidia_Linux_driver_under_development|http://digg.com/linux_unix/3D_Nvidia_Linux_driver_under_development]] Discussion at digg. 
* _Oct 14, 2006_ [[http://www.freesoftwaremagazine.com/node/1797|http://www.freesoftwaremagazine.com/node/1797]] Mitch Meyran's blog entry 
* _Jul 19, 2006_ [[http://lwn.net/Articles/191900/|http://lwn.net/Articles/191900/]] LWN: Ottawa Linux Symposium, about "Open source graphic drivers - they don't kill kittens" 