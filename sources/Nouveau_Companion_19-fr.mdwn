[[!table header="no" class="mointable" data="""
 [[Accueil|FrontPage-fr]]  |  [[TiNDC 2006|IrcChatLogs-fr]]  |  [[TiNDC 2007|IrcChatLogs-fr]]  |  [[Archives anciennes|IrcChatLogs-fr]]  |  [[Archives récentes|IrcChatLogs-fr]]  | DE/[[EN|Nouveau_Companion_19]]/[[ES|Nouveau_Companion_19-es]]/FR/RU/[[Team|Translation_Team]] 
"""]]


## Le compagnon irrégulier du développement de Nouveau (TiNDC)


## Édition du 12 mai 2007

[!] La traduction est terminée, mais une relecture ne serait pas de refus 
### Introduction

Bonjour une nouvelle fois pour le 19ème numéro du TiNDC. 

Il semble que le gestionnaire TTM a été posté à la _mailing-list_ du noyau Linux pour évaluation. Si tout se passe bien, il sera intégré au noyau. Malheureusement pour nous, le TTM semble attendre exactement une FIFO en espace noyau alors que nouveau a au moins 16 FIFOs en espace utilisateur. Alors que faire maintenant? 

* nous allons demander des modifications au cœur du code de TTM. 
* nous allons devoir implémenter le code du driver Nouveau pour l'interfacer avec TTM. 
* nous allons devoir implémenter le code en espace utilisateur pour utiliser TTM. 
Dans tous les cas, le _mode setting_ en espace noyau est inclus dans le patch TTM. Nous avons déjà couvert ce sujet dans un précédent TiNDC. 

L'évaluation a été globalement positive, mais quelques modifications sont requises dans la zone des ioctl(), du _coding style_ et de la documentation. Le code est revenu dans le territoire des _hackers_ Xorg/DRM, sera modifié et retournera alors sur la _mailing-list_ noyeau pour évaluation. En fait, il semble que les gars sur #dri-devel veulent une modifications plus importante (Il semble qu'Intel souhaite aussi avoir plus d'une seule FIFO). L'un dans l'autre, il semble qu'un délai plus important se profile à l'horizon. 

Notre projet _Vacation of Code_ démarrera début juin et durera jusqu'à août. Ahuilet, qui essaie d'améliorer notre support xv a accepté de nous donner un compte rendu pour chaque TiNDC qui sera publié durant cette période. Merci ahuillet! 

Il semble que Nvidia a mis de côté XvMX pour ses G8x, ou va abandonner son implémentation. Cela pourrait être la première fonctionnalité disponible uniquement dans Nouveau. 

Pour l'instant, le déroulement et le travail est relativement lent, puisque marcheu et darktama sont assez occupés tous les deux avec des choses de la vraie vie. Cependant, à partir du mois prochain les choses vont heureusement s'accélérer à nouveau. 

Nous attendons toujours la décision finale sur l'acceptation de notre projet à l' _American Software Conservancy_. C'est pour cela que nos développeurs sont livrés à eux-même concernant leurs achats de matériel, mais petit à petit les cartes à base de G8x arrivent dans nos rangs ;) Cependant, ne retenez pas votre souffle plus longtemps à propos d'un driver qui marche, comme expliqué plus loin il y a encore beaucoup de travail. 


### État actuel

Dans notre dernière édition, nous avons noté que le port pour BSD était bloqué car il manquait au DRM quelques fonctionnalités importantes. Déjà avant que la dernière édition soit publiée, camje_lemon avait tenté sa chance pour faire marcher Nouveau. Peu de temps après avoir publié le dernier TiNDC, il y arriva. Vous pouvez maintenant vous réjouir, fans de *BSD (et nous aider à porter et tester). 

jwstokl a encore travaillé sur le _macro finder_. Après avoir piétiné plusieurs problèmes de _freeze_, il demanda à pq et z3ro de tester. PQ a eu quelques suggestions sur comment améliorer le design, que jwstolk a pris en considération. Marcheu a bien aimé l'idée. 

Ahuillet a commencé les préparatifs pour son projet _Vacation of Code_, sponsorisé par Xorg. Il a étudié le code et a clamé qu'il ignorait tout à propos des différents problèmes à portée de main. Cependant, il a posé quelques questions spécifiques à propos du code un peu plus tard, ce qui a réellement remis en doute son ignorance. 

Nous avons maintenant un mailing list [[ici|http://lists.freedesktop.org/mailman/listinfo/nouveau]]. Vous pouvez nous envoyez les bugs et tout ce qui concerne nouveau là. CJB a été suffisamment insistant pour que tout ça soit mis en place. Cependant, notre attention particulière sera toujours le canal IRC. Quoi qu'il en soit, utilisez-la. 

Le problème avec les cartes NV15 est finalement résolu : lors de l'émission de commandes de rendu 3D (par exemple pendant glxgears), la machine se bloquait complètement. Cela était causé par une tempête d'interruptions qui déchirait complètement le système. Après quelques aiguillages, matc a trouvé l'erreur: nous initialisions incorrectement le contexte. Au lieu d'utiliser une taille de 32, nous initialisions 64bits. Abotezatu fut volontaire pour testé ce patch et nous a rapporté que c'était un succès. Quelques jours plus tard, pmdata tenta sa chance lui aussi et confirma cela, cependant maintenant X crashe pour lui lorsqu'il clique en dehors de la fenêtre de glxgears. 

pq et z3ro ont continué à finaliser leurs spécifications de la base de données _rules-ng_. Nous avons maintenant à disposition une documentation sur les _tags_ disponibles. Un travail plus poussé ira dans la création des outils requis pour aller avec ces spécifications. La plupart de la discussion s'est finalement déplacé sur #dri-devel car les gens de Radeon essayent d'utiliser _rules-ng_ pour leur matériel aussi. 

Darktama a finalement craqué et a acheté un nouveau système incluant une NVidia 8600GT. Une première analyse des traces mmio ont révélé que les 8600 sont vraiment différentes des cartes produites précédemment. Et quand Wallbraker a offert les dumps de sa toute nouvelle 8800 GTS il a été révélé qu'il y avait même quelques petites différences entre G80 et G84. 

Il semble que le driver beta actuel a des problèmes avec quelques _vertex programs_ (par exemple vertex_program3() dans renouveau), ou que nous ne savons pas coder des _vertex programs_ correctement. Le résultat est que renouveau crashe pendant vertex_program3(). Vous pouvez commenter ce test si renouveau crashe votre carte G8x. 

Actuellement, rien ne marche concernant ces cartes. La 2D aura tout d'abord besoin d'être travaillée. La 3D est une autre histoire. Nous sommes presque surs que nous pouvons soit le faire correctement et découvrir comment ces nouvelles cartes marchent, ou nous pouvons essayer de tricher et utiliser une sorte de mode de compatibilité (avec les NV4x), ce qui permettrait d'avoir des résultats plus tôt, mais qui serait une voie sans issue à long terme pour le développement futur de notre driver. 

À nouveau, encore du développement dans la branche Randr1.2. Airlied a ajouté un peu de code de configuration, a enlevé un peu de code inutilisé et a, on l'espère, réparé quelques bugs. 


### Aide requise

Envoyez vos dumps pour le SLI et les 8x00. Malheureusement nous n'avons pas pas de liste actuellement disponible, mais restez assurés que vos dumps ne seront pas perdus. 

En outre, nous cherchons des dumps MMio de cartes qui ne font pas tourner glxgears correctement. 

Si vous avez une NV15, testez nos dernières modifications! 

[[<<< Édition précédente|Nouveau_Companion_18-fr]] | [[Édition suivante >>>|Nouveau_Companion_20-fr]] 
